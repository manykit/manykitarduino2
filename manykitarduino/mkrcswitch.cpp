// mkrcswitch.cpp

#include "mkrcswitch.h"

#if defined MK_RCSWITCH

#include "mkarduino.h"

//----------------------------------------------------------------------------
mkRCSwitch::mkRCSwitch()
{
}
//----------------------------------------------------------------------------
mkRCSwitch::~mkRCSwitch()
{
}
//----------------------------------------------------------------------------
mkObject *mkRCSwitch::Create(unsigned char cmdCH, String *cmdParams)
{
    if (mkArduino::sOptTypeVal[OT_RC_INIT]==cmdCH)
    {
        String name = cmdParams[1];
        if (!mkArduino::ardu->GetObjectByNameUni(sGenName(name)))
        {
            mkObject *obj = new mkRCSwitch();
            obj->OnCreate(name, cmdCH, cmdParams);
            return obj;   
        }
    }

    return 0;
}
//----------------------------------------------------------------------------
void mkRCSwitch::Update()
{
    mkObject::Update();

    if (mRCSwitch.available()) 
    {   
        int recvVal = mRCSwitch.getReceivedValue();

        unsigned char cmdCh = mkArduino::sOptTypeVal[OT_RETRUN_RC];
        char strCMDCh[32];
        memset(strCMDCh, 0, 32);
        itoa(cmdCh, strCMDCh, 10);
        
        Serial.print("0000");
        Serial.print(String(strCMDCh)); 
        Serial.print(" ");
        Serial.print(Name);
        Serial.print(" ");
        Serial.println(recvVal);

        mRCSwitch.resetAvailable();
    }
}
//----------------------------------------------------------------------------
void mkRCSwitch::OnCMD(unsigned char cmdCH, String *cmdParams)
{
    if (mkArduino::sOptTypeVal[OT_RC_INIT]==cmdCH)
    {      
        int pin = mkArduino::_Str2Pin(cmdParams[2]);
        RCInit(pin);
    }
    else if (mkArduino::sOptTypeVal[OT_RC_SEND]==cmdCH)
    {
        long val = mkArduino::_Str2Long(cmdParams[2]);
        RCSend(val);
    }
}
//----------------------------------------------------------------------------
void mkRCSwitch::InitRCSwitchReceive(int pinTimerIndex)
{
    mRCSwitch.enableReceive(pinTimerIndex); 
}
//----------------------------------------------------------------------------
void mkRCSwitch::RCInit(int pin)
{
    mRCSwitch.enableTransmit(pin);
}
//----------------------------------------------------------------------------
void mkRCSwitch::RCSend(long val)
{
    mRCSwitch.send(val, 24);
    delay(100);
    mRCSwitch.send(val, 24);
    delay(100);
    mRCSwitch.send(val, 24);
}
//----------------------------------------------------------------------------

#endif