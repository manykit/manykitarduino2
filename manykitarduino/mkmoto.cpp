// mkmoto.cpp

#include "mkmoto.h"

#if defined MK_MOTO

#include "mkarduino.h"

#if defined MK_PID

double Kp = 0.6, Ki = 5, Kd = 0;

void wheelSpeedL()
{
    mkMoto *moto = (mkMoto*)(mkArduino::ardu->moto);

    int lstate = digitalRead(moto->mPinEncroderLA);
    if((moto->mEncoder0PinALastL == LOW) && lstate==HIGH)
    {
        int val = digitalRead(moto->mPinEncroderLB);
        if(val == LOW && moto->mDirectionL)
        {
            moto->mDirectionL = false; //Reverse
        }
        else if(val == HIGH && !moto->mDirectionL)
        {
            moto->mDirectionL = true;  //Forward
        }
    }
    moto->mEncoder0PinALastL = lstate;
    
    if(!moto->mDirectionL) 
        moto->mDurationLTemp++;
    else  
        moto->mDurationLTemp--;
}
void wheelSpeedR()
{
    mkMoto *moto = (mkMoto*)(mkArduino::ardu->moto);

    int lstate = digitalRead(moto->mPinEncroderRA);
    if((moto->mEncoder0PinALastR == LOW) && lstate==HIGH)
    {
        int val = digitalRead(moto->mPinEncroderRB);
        if(val == LOW && !moto->mDirectionR)
        {
            moto->mDirectionR = true; //Reverse
        }
        else if(val == HIGH && moto->mDirectionR)
        {
            moto->mDirectionR = false;  //Forward
        }
    }
    moto->mEncoder0PinALastR = lstate;
    
    if(!moto->mDirectionR)
        moto->mDurationRTemp++;
    else 
        moto->mDurationRTemp--;
}
#endif
//----------------------------------------------------------------------------
mkMoto::mkMoto()
{
    mMotoMode = MM_BOARD;

    mPinL0 = 0;
    mPinL1 = 0;
    mPinLSpeed = 0;
    mPinR0 = 0;
    mPinR1 = 0;
    mPinRSpeed = 0;
    mIsUseSpeedEncorder = false;

    mPinEncroderLA = 2;
    mPinEncroderLB = 8;
    mPinEncroderRA = 3;
    mPinEncroderRB = 9;

#if defined MK_PID
    mLastSendTime = 0;
    mMotoResultL = false;
    mMotoResultR = false;

    mEncoder0PinALastL = 0;
    mEncoder0PinALastR = 0;
    mDurationLTemp = 0;
    mDurationRTemp = 0;
    mAbsDurationL = 0;
    mAbsDurationR = 0;
    mIsStopL = true;
    mIsStopR = true;
    mValOutputL = 0;
    mValOutputR = 0;
    mDirectionL = true;
    mDirectionR = true;

    mPID = 0;
    mPID1 = 0;
#endif

    mkArduino::ardu->moto = this;
}
//----------------------------------------------------------------------------
mkMoto::~mkMoto()
{
}
//----------------------------------------------------------------------------
mkObject *mkMoto::Create(unsigned char cmdCH, String *cmdParams)
{
    if (mkArduino::sOptTypeVal[OT_MOTO_I]==cmdCH)
    {
        String name = cmdParams[1];
        if (!mkArduino::ardu->GetObjectByNameUni(sGenName(name)))
        {
          mkObject *obj = new mkMoto();
          obj->OnCreate(name, cmdCH, cmdParams);
          return obj;
        }
    }

    return 0;
}
//----------------------------------------------------------------------------
void mkMoto::OnSendBack()
{
#if defined MK_PID
    if (mIsUseSpeedEncorder && mPID && mPID1)
    {
        if (!mIsStopL)
        analogWrite(mPinLSpeed, (int)mValOutputL);
        if (!mIsStopR)
        analogWrite(mPinRSpeed, (int)mValOutputR);

        mAbsDurationL = abs(mDurationLTemp);
        mAbsDurationR = abs(mDurationRTemp);

        mMotoResultL = mPID->Compute();
        mMotoResultR = mPID1->Compute();

        if (mMotoResultL)
            mDurationLTemp = 0;
        if (mMotoResultR)
            mDurationRTemp = 0;

        if (millis() - mLastSendTime >= mk_pid_sendspeedtime)
        {
            mLastSendTime = millis();
            _SendbackSpeed();
        }
    }
#endif
}
//----------------------------------------------------------------------------
void mkMoto::OnCMD(unsigned char cmdCH, String *cmdParams) 
{
    if (mkArduino::sOptTypeVal[OT_MOTO_I]==cmdCH)
    {
        Init10111213();
    }
    else if (mkArduino::sOptTypeVal[OT_MOTO_I_DRIVER4567] == cmdCH)
    {
        Init4567();
    }
    else if (mkArduino::sOptTypeVal[OT_MOTO_I_DRIVER298N] == cmdCH)
    {
        int pinL = mkArduino::_Str2Pin(cmdParams[2]);
        int pinL1 = mkArduino::_Str2Pin(cmdParams[3]);
        int pinLS = mkArduino::_Str2Pin(cmdParams[4]);
        int pinR = mkArduino::_Str2Pin(cmdParams[5]);
        int pinR1 = mkArduino::_Str2Pin(cmdParams[6]);
        int pinRS = mkArduino::_Str2Pin(cmdParams[7]);      
        Init298N(pinL, pinL1, pinLS, pinR, pinR1, pinRS);
    }   
    else if (mkArduino::sOptTypeVal[OT_MOTO_RUN]==cmdCH)
    {
        int motoIndex = mkArduino::_Str2Int(cmdParams[2]);
        int dir = mkArduino::_Str2DirType(cmdParams[3]);
        int spd = mkArduino::_Str2Int(cmdParams[4]);

        if (0 == dir)
            spd = 0;

        if (0 == motoIndex)
            RunLeft(dir, spd);
        else if (1 == motoIndex)
            RunRight(dir, spd);
    }
    else if (mkArduino::sOptTypeVal[OT_MOTO_RUNSIMPLE]==cmdCH)
    {
        int dir = mkArduino::_Str2SimpleDirType(cmdParams[2]);
        int spd = mkArduino::_Str2Int(cmdParams[3]);
        if (0 == dir)
        {
            spd = 0;
            RunLeft(0, spd);
            RunRight(0, spd);
        }
        else if (1 == dir)
        {
            RunLeft(1, spd);
            RunRight(1, spd);
        }
        else if (2 == dir)
        {
            RunLeft(2, spd);
            RunRight(2, spd);
        }
        else if (3 == dir)
        {
            RunLeft(2, spd);
            RunRight(1, spd);
        }
        else if (4 == dir)
        {
            RunLeft(1, spd);
            RunRight(2, spd);
        }
    }
    else if (mkArduino::sOptTypeVal[OT_MOTO_STOP]==cmdCH)
    {
        RunLeft(0, 0);
        RunRight(0, 0);
    }
    else if (mkArduino::sOptTypeVal[OT_MOTO_I_SPD]==cmdCH)
    {
#if defined MK_PID
        int pinLA = mkArduino::_Str2Pin(cmdParams[2]);
        int pinLB = mkArduino::_Str2Pin(cmdParams[3]);
        int pinRA = mkArduino::_Str2Pin(cmdParams[4]);
        int pinRB = mkArduino::_Str2Pin(cmdParams[5]);
        PidInit(pinLA, pinLB, pinRA, pinRB);
#endif
    }
}
//----------------------------------------------------------------------------
void mkMoto::Init10111213()
{
  mPinL0 = 12;
  mPinL1 = 12;
  mPinR0 = 13;
  mPinR1 = 13;
  mPinLSpeed = 10;
  mPinRSpeed = 11;

  pinMode(mPinL0, OUTPUT);
  pinMode(mPinR0, OUTPUT);
  pinMode(mPinLSpeed, OUTPUT);
  pinMode(mPinRSpeed, OUTPUT);

  mMotoMode = MM_BOARD;
  mIsUseSpeedEncorder = false;
}
//----------------------------------------------------------------------------
void mkMoto::Init4567()
{
  mPinL0 = 4;
  mPinL1 = 4;
  mPinR0 = 7;
  mPinR1 = 7;
  mPinLSpeed = 5;
  mPinRSpeed = 6;

  pinMode(mPinL0, OUTPUT);
  pinMode(mPinR0, OUTPUT);
  pinMode(mPinLSpeed, OUTPUT);
  pinMode(mPinRSpeed, OUTPUT);

  mMotoMode = MM_BOARD;
  mIsUseSpeedEncorder = false;
}
//----------------------------------------------------------------------------
void mkMoto::Init298N(int pinL, int pinL1, int pinLS, int pinR, 
  int pinR1, int pinRS)
{
  mPinL0 = pinL;
  mPinL1 = pinL1;
  mPinLSpeed = pinLS;
  
  mPinR0 = pinR;
  mPinR1 = pinR1;
  mPinRSpeed = pinRS;

  pinMode(mPinL0, OUTPUT);
  pinMode(mPinL1, OUTPUT);
  pinMode(mPinLSpeed, OUTPUT);
    
  pinMode(mPinR0, OUTPUT);
  pinMode(mPinR1, OUTPUT);
  pinMode(mPinRSpeed, OUTPUT);

  mMotoMode = MM_298N;
  mIsUseSpeedEncorder = false;
}
//----------------------------------------------------------------------------
void mkMoto::PidInit(int encorderLA, int encorderLB,
  int encorderRA, int encorderRB)
{
  mPinEncroderLA = encorderLA;
  mPinEncroderLB = encorderLB;
  mPinEncroderRA = encorderRA;
  mPinEncroderRB = encorderRB;

  if (encorderLA == encorderLB && encorderRA==encorderRB)
  {
    mIsUseSpeedEncorder = false;

#if defined MK_PID
    if (mPID)
    {
      delete(mPID);
    }
    mPID = 0;
    if (mPID1)
    {
      delete(mPID1);
    }
    mPID1 = 0;
    mEncoder0PinALastL = 0;
    mEncoder0PinALastR = 0;
    mDurationLTemp = 0;
    mDurationRTemp = 0;
    mAbsDurationL = 0;
    mAbsDurationR = 0;
    mValOutputL = 0;
    mValOutputR = 0;
    mDirectionL = true;
    mDirectionR = true;
#endif
  }
  else
  {
    pinMode(mPinEncroderLB, INPUT);
    pinMode(mPinEncroderRB, INPUT);

    mIsUseSpeedEncorder = true;

#if defined MK_PID
    mEncoder0PinALastL = 0;
    mEncoder0PinALastR = 0;
    mDurationLTemp = 0;
    mDurationRTemp = 0;
    mAbsDurationL = 0;
    mAbsDurationR = 0;
    mValOutputL = 0;
    mValOutputR = 0;
    mDirectionL = true;
    mDirectionR = true;

    if (mPID)
    {
      delete(mPID);
      mPID = 0;
    }
    mPID = new PID(&mAbsDurationL, &mValOutputL, &mSetPoint_L, Kp, Ki, Kd, DIRECT);

    if (mPID1)
    {
      delete(mPID1);
      mPID1 = 0;
    }
    mPID1 = new PID(&mAbsDurationR, &mValOutputR, &mSetPoint_R, Kp, Ki, Kd, DIRECT);

    mPID->SetMode(AUTOMATIC);
    mPID->SetSampleTime(100);
    mPID1->SetMode(AUTOMATIC);
    mPID1->SetSampleTime(100);
    
    mDirectionL = true;
    pinMode(mPinEncroderLB, INPUT);  
    if (2 == encorderLA)
    {
      attachInterrupt(0, wheelSpeedL, CHANGE);
    }
    if (3 == encorderLA)
    {
      attachInterrupt(1, wheelSpeedL, CHANGE);
    }
  
    mDirectionR = true;
    pinMode(mPinEncroderRB, INPUT);  
    if (3 == encorderRA)
    {
      attachInterrupt(1, wheelSpeedR, CHANGE);
    }
    if (21 == encorderRA)
    {
      attachInterrupt(2, wheelSpeedR, CHANGE);
    }
    if (20 == encorderRA)
    {
      attachInterrupt(3, wheelSpeedR, CHANGE);
    }
    if (19 == encorderRA)
    {
      attachInterrupt(4, wheelSpeedR, CHANGE);
    }
    if (18 == encorderRA)
    {
      attachInterrupt(5, wheelSpeedR, CHANGE);
    }
#endif 
  }
}
//----------------------------------------------------------------------------
void mkMoto::PidShutdown()
{
#if defined MK_PID
    if (mPID)
    {
        delete(mPID);
    }
    mPID = 0;

    if (mPID1)
    {
        delete(mPID1);
    }
    mPID1 = 0;
#endif
}
//----------------------------------------------------------------------------
void mkMoto::Run(int leftright, int dir, int spd)
{
    if (0 == dir)
        spd = 0;

    if (0 == leftright)
    {
        RunLeft(dir, spd);
    }
    else if (1 == leftright)
    {
        RunRight(dir, spd);
    }
}
//----------------------------------------------------------------------------
void mkMoto::RunLeftRight(int dir, int spd)
{
    if (0 == dir)
    {
        spd = 0;
        RunLeft(0, spd);
        RunRight(0, spd);
    }
    else if (1 == dir)
    {
        RunLeft(1, spd);
        RunRight(1, spd);
    }
    else if (2 == dir)
    {
        RunLeft(2, spd);
        RunRight(2, spd);
    }
    else if (3 == dir)
    {
        RunLeft(2, spd);
        RunRight(1, spd);
    }
    else if (4 == dir)
    {
        RunLeft(1, spd);
        RunRight(2, spd);
    }
}
//----------------------------------------------------------------------------
void mkMoto::Stop()
{
    RunLeft(0, 0);
    RunRight(0, 0);
}
//----------------------------------------------------------------------------
void mkMoto::RunLeft(int val, int spd)
{
  if (MM_BOARD == mMotoMode)
  {
    if (mIsUseSpeedEncorder)
    {
      mSetPoint_L = spd;

      if (0 == val)
      {
        analogWrite(mPinLSpeed, 0);
        mSetPoint_L = 0;
        #if defined MK_PID
        mIsStopL=true;
        #endif
      }
      else if (1 == val)
      {
        digitalWrite(mPinL0, HIGH);
        #if defined MK_PID
        mIsStopL=false; 
        #endif
      }
      else if (2 == val)
      {
        digitalWrite(mPinL0, LOW);
        #if defined MK_PID
        mIsStopL=false;
        #endif
      }
    }
    else
    {
      mSetPoint_L = spd;
      analogWrite(mPinLSpeed, (int)mSetPoint_L);

      if (0 == val)
      {
        #if defined MK_PID
        mIsStopL=true;
        #endif
      }
      if (1 == val)
      {
        digitalWrite(mPinL0, HIGH);
        #if defined MK_PID
        mIsStopL=false; 
        #endif
      }
      else if (2 == val)
      {
        digitalWrite(mPinL0, LOW); 
        #if defined MK_PID
        mIsStopL=false; 
        #endif
      }
    }
  }
  else if (MM_298N == mMotoMode)
  {
    if (mIsUseSpeedEncorder)
    {
      mSetPoint_L = spd;
      
      if (0 == val)
      {
        digitalWrite(mPinL0, LOW);
        digitalWrite(mPinL1, LOW);
        analogWrite(mPinLSpeed, 0);
        mSetPoint_L = 0;
#if defined MK_PID
        mIsStopL=true;
#endif
      }
      else if (1 == val)
      {
        digitalWrite(mPinL0, HIGH);
        digitalWrite(mPinL1, LOW);
#if defined MK_PID
        mIsStopL=false;
#endif
      }
      else if (2 == val)
      {
        digitalWrite(mPinL0, LOW);
        digitalWrite(mPinL1, HIGH);
#if defined MK_PID
        mIsStopL=false; 
#endif
      }
    }
    else
    {
      mSetPoint_L = spd;
      
      if (0 == val)
      {
        digitalWrite(mPinL0, LOW);
        digitalWrite(mPinL1, LOW);
        mSetPoint_L = 0;

#if defined MK_PID
        mIsStopL=true; 
#endif
      }
      else if (1 == val)
      {
        digitalWrite(mPinL0, HIGH);
        digitalWrite(mPinL1, LOW);

#if defined MK_PID
        mIsStopL=false; 
#endif
      }
      else if (2 == val)
      {
        digitalWrite(mPinL0, LOW);
        digitalWrite(mPinL1, HIGH);

#if defined MK_PID
        mIsStopL=false; 
#endif
      }

      analogWrite(mPinLSpeed, (int)mSetPoint_L);
    }
  }
}
//----------------------------------------------------------------------------
void mkMoto::RunRight(int val, int spd)
{
  if (MM_BOARD == mMotoMode)
  {
    if (mIsUseSpeedEncorder)
    {
      mSetPoint_R = spd;

      if (0 == val)
      {
        analogWrite(mPinRSpeed, 0); 
#if defined MK_PID
        mIsStopR=true;
#endif
        mSetPoint_R = 0;
      }
      else if (1 == val)
      {
        digitalWrite(mPinR0, HIGH);
#if defined MK_PID
       mIsStopR=false; 
#endif
      }
      else if (2 == val)
      {
        digitalWrite(mPinR0, LOW); 
#if defined MK_PID
        mIsStopR=false; 
#endif
      }
    }
    else
    {
      mSetPoint_R = spd;
      analogWrite(mPinRSpeed, (int)mSetPoint_R);

      if (0 == val)
      {
#if defined MK_PID
        mIsStopR=true;
#endif
      }
      if (1 == val)
      {
        digitalWrite(mPinR0, HIGH);
#if defined MK_PID
        mIsStopR=false; 
#endif
      }
      else if (2 == val)
      {
        digitalWrite(mPinR0, LOW); 
#if defined MK_PID
        mIsStopR=false; 
#endif
      }
    }
  }
  else if (MM_298N == mMotoMode)
  {
    if (mIsUseSpeedEncorder)
    { 
      mSetPoint_R = spd;       
          
      if (0 == val)
      {
        digitalWrite(mPinR0, LOW);
        digitalWrite(mPinR1, LOW);
        analogWrite(mPinRSpeed, 0); 
        mSetPoint_R = 0;
#if defined MK_PID
        mIsStopR=true;
#endif
      }
      else if (1 == val)
      {
        digitalWrite(mPinR0, LOW);
        digitalWrite(mPinR1, HIGH);
#if defined MK_PID
        mIsStopR=false;
#endif
      }
      else if (2 == val)
      {
        digitalWrite(mPinR0, HIGH);
        digitalWrite(mPinR1, LOW);

#if defined MK_PID
        mIsStopR=false;
#endif
      }
    }
    else
    {
      mSetPoint_R = spd;

      if (0 == val)
      {
        digitalWrite(mPinR0, LOW);
        digitalWrite(mPinR1, LOW);
        mSetPoint_R = 0;
        
#if defined MK_PID
        mIsStopR=true;
#endif
      }
      else if (1 == val)
      {
        digitalWrite(mPinR0, LOW);
        digitalWrite(mPinR1, HIGH);

#if defined MK_PID
        mIsStopR=false;
#endif
      }
      else if (2 == val)
      {
        digitalWrite(mPinR0, HIGH);
        digitalWrite(mPinR1, LOW);
        
#if defined MK_PID
        mIsStopR=false;
#endif
      }

      analogWrite(mPinRSpeed, (int)mSetPoint_R);
    }
  }
}

//----------------------------------------------------------------------------
void mkMoto::_SendbackSpeed()
{
#if defined MK_PID
  unsigned char cmdCh = mkArduino::sOptTypeVal[OT_RETURN_MOTOSPD];
  char strCMDCh[32];
  memset(strCMDCh, 0, 32);
  itoa(cmdCh, strCMDCh, 10);

  Serial.print("0000");
  Serial.print(String(strCMDCh));
  Serial.print(" ");
  Serial.print(Name);
  Serial.print(" ");
  Serial.print(mAbsDurationL);
  Serial.print(" ");
  Serial.print(mDirectionL ? 1:0);
  Serial.print(" ");
  Serial.println(mAbsDurationR);
  Serial.print(" ");
  Serial.println(mDirectionR ? 1:0);
#endif
}
//----------------------------------------------------------------------------
float mkMoto::GetSpeedLeft() const
{
  return mAbsDurationL;
}
//----------------------------------------------------------------------------
float mkMoto::GetSpeedRight() const
{
  return mAbsDurationR;
}
//----------------------------------------------------------------------------
int mkMoto::GetDirectionLeft() const
{
  return mDirectionL ? 1:0;
}
//----------------------------------------------------------------------------
int mkMoto::GetDirectionRight() const
{
  return mDirectionR ? 1:0;
}
//----------------------------------------------------------------------------

#endif