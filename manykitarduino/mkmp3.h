// mkmp3.h

#ifndef MKMP3_H
#define MKMP3_H

#include "mkarduinodef.h"

#if defined MK_MP3

#include "mkobject.h"
#include "_mkmp3kt403a.h"

enum Mp3PlayType
{
  MPT_PLAY,
  MPT_PAUSE,
  MPT_STOP,
  MPT_NEXT,
  MPT_BEFORE,
  MPT_RANDOM,
  MPT_LOOP_SINGLE,
  MPT_LOOP_SINGLE_CLOSE,
  MPT_LOOP_ALL,
  MPT_LOOP_ALL_CLOSE,
  MPT_VOLUME_INCREASE,
  MPT_VOLUME_DECREASE,
  MPT_MAX_TYPE
};

class mkMP3 : public mkObject
{
public:
    mkMP3();
    virtual ~mkMP3();

    static String sGenName(const String &t) { return "7_" + t; }
    virtual String GenName(const String &t) { return "7_" + t; }

    static mkObject *Create(unsigned char cmdCH, String *cmdParams);

    virtual void OnCMD(unsigned char cmdCH, String *cmdParams);

    void Init(int pinR, int pinT);
    void Do(Mp3PlayType type);
    void FolderPlay(int folder, int index);
    void SetVolume(int val);

private:
    MP3 mMP3;
};

#endif

#endif
