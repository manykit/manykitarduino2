// mkscreeni2c.h

#ifndef MKSCREENI2C_H
#define MKSCREENI2C_H

#include "mkarduinodef.h"

#if defined MK_SCREEN_I2C

#include "mkobject.h"
#include "_mkliquidcrystali2c.h"

enum SCREEN_I2C_DoType
{
  SCR_INIT,
  SCR_CLEAR,
  SCR_HOME,
  SCR_NO_DISPLAY,
  SCR_DISPLAY,
  SCR_NO_BLINK,
  SCR_BLINK,
  SCR_NO_CURSOR,
  SCR_CURSOR,
  SCR_SCROOL_DISPLAYLEFT,
  SCR_SCROOL_DISPLAYRIGHT,
  SCR_LEFT_TO_RIGHT,
  SCR_RIGHT_TO_LEFT,
  SCR_NO_BACKLIGHT,
  SCR_BACKLIGHT,
  SCR_NO_AUTO_SCROOL,
  SCR_AUTO_SCROLL,
  SCR_BLINK_ON,
  SCR_BLINK_OFF,
  SCR_CURSOR_ON,
  SCR_CURSOR_OFF,
  SCR_ON,
  SCR_OFF,
  SCR_MAX_TYPE
};

class mkScreenI2C : public mkObject
{
public:
    mkScreenI2C();
    virtual ~mkScreenI2C();

    static String sGenName(const String &t) { return "10_" + t; }
    virtual String GenName(const String &t) { return "10_" + t; }

    static mkObject *Create(unsigned char cmdCH, String *cmdParams);

    virtual void OnCMD(unsigned char cmdCH, String *cmdParams);

    void Init(int addr = 0x3F, int numCols=16, int numRows=2);
    void DoType(SCREEN_I2C_DoType doType);
    void Do(int doType);
    void SetCursor(int col, int row);
    void SetBackLight(int val);
    void Print(const String &val);
    void PrintByte(int selfCreateCharIndex);

private:
    LiquidCrystal_I2C *mLiquidCrystal_I2C;
};

#endif

#endif
