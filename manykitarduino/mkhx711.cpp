// mkhx711.cpp

#include "mkhx711.h"

#if defined MK_XH711

#include "mkarduino.h"

//----------------------------------------------------------------------------
mkHX711::mkHX711()
{
}
//----------------------------------------------------------------------------
mkHX711::~mkHX711()
{
}
//----------------------------------------------------------------------------
mkObject *mkHX711::Create(unsigned char cmdCH, String *cmdParams)
{
    if (mkArduino::sOptTypeVal[OT_HX711_I]==cmdCH)
    {
        String name = cmdParams[1];
        if (!mkArduino::ardu->GetObjectByNameUni(sGenName(name)))
        {
          mkObject *obj = new mkHX711();
          obj->OnCreate(name, cmdCH, cmdParams);
          return obj;
        }
    }

    return 0;
}
//----------------------------------------------------------------------------
void mkHX711::OnCMD(unsigned char cmdCH, String *cmdParams)
{
    if (mkArduino::sOptTypeVal[OT_HX711_I]==cmdCH)
    {
      int pinOut = mkArduino::_Str2Pin(cmdParams[2]);
      int pinClk = mkArduino::_Str2Pin(cmdParams[3]);
      HX711Init(pinOut, pinClk);
    }
    else if (mkArduino::sOptTypeVal[OT_HX711_TEST]==cmdCH)
    {
      float val = _ReadHX711();
      _HXSend(val);
    }
}
//----------------------------------------------------------------------------
void mkHX711::HX711Init(int pinOut, int pinClk)
{
  mXH711 = HX711(pinOut, pinClk);
}
//----------------------------------------------------------------------------
float mkHX711::_ReadHX711()
{
    float val = mXH711.read_average(4); 
    return val;
}
//----------------------------------------------------------------------------
void mkHX711::_HXSend(float val)
{
    unsigned char cmdCh = mkArduino::sOptTypeVal[OT_RETURN_HX711];
    char strCMDCh[32];
    memset(strCMDCh, 0, 32);
    itoa(cmdCh, strCMDCh, 10);

    Serial.print("0000");
    Serial.print(String(strCMDCh)); 
    Serial.print(" ");
    Serial.print(Name);
    Serial.print(" ");
    Serial.println(val);
}
//----------------------------------------------------------------------------

#endif