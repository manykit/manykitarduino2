// mkarduinocfg.h

#ifndef MKARDUINOCFG_H
#define MKARDUINOCFG_H

// define this, send version every 2 sconds(just like "heart")
#define MK_SENDVERSION 1

// motopid
#define MK_MOTO 1
#define MK_PID 1
#define mk_pid_sendspeedtime 100

#define MK_DHT 1
#define MK_XH711 1
#define MK_IR 1
#define MK_DIST 1
//#define MK_LEDMATRIX 1
//#define MK_SCREEN_I2C 1
//#define MK_MP3 1
#define MK_RGBLED 1
//#define MK_RCSWITCH 1
#define MK_SEGMENT 1
#define MK_SERVO 1

#define MK_DUST 1

#endif
