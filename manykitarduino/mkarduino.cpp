// mkarduino.cpp

#include "mkarduino.h"

#if defined MK_SERVO
#include "mkservo.h"
#endif

#if defined MK_DHT
#include "mkdht.h"
#endif

#if defined MK_DIST
#include "mkdist.h"
#endif

#if defined MK_DUST
#include "mkdust.h"
#endif

#if defined MK_IR
#include "mkir.h"
#endif

#if defined MK_LEDMATRIX
#include "mkledmatrix.h"
#endif

#if defined MK_MOTO
#include "mkmoto.h"
#endif

#if defined MK_MP3
#include "mkmp3.h"
#endif

#if defined MK_RGBLED
#include "mkrgbled.h"
#endif

#if defined MK_SCREEN_I2C
#include "mkscreeni2c.h"
#endif

#if defined MK_SEGMENT
#include "mksegment.h"
#endif

mkArduino mk;

//----------------------------------------------------------------------------
String mkArduino::Useage = String("def");
mkArduino *mkArduino::ardu = 0;
//----------------------------------------------------------------------------
char mkArduino::PinStr[P_MAX_TYPE] =
{
  0,
  1,
  2,
  3,
  4,
  5,
  6,
  7,
  8,
  9,
  10,
  11,
  12,
  13,
  14,
  15,
  16,
  17,
  18,
  19,
  20,
  21,
  30,
  31,
  32,
  33,
  34,
  35,
  36,
  37,
  38,
  39,
  40,
  41,
  42,
  43,
  44,
  45,
};
//----------------------------------------------------------------------------
unsigned char mkArduino::sOptTypeVal[OT_MAX_TYPE] =
{
  100, //OT_TOGET_NETID
  101, //OT_RETRUN_NETID
  0,   //OT_PM
  1,   //OT_DW
  2,   //OT_AW
  3,   //OT_RETURN_DR
  4,   //OT_RETURN_AR
  5,   //OT_SVR_I
  6,   //OT_SVR_W
  7,   //OT_DST_I
  8,   //OT_DST_T
  9,   //OT_RETURN_DIST
  10,  //OT_MOTO_I
  11,  //OT_MOTO_RUN
  12,  //OT_MOTO_RUNSIMPLE
  13,  //OT_MOTO_STOP
  14,  //OT_MOTO_I_SPD
  15,  //OT_RETURN_MOTOSPD
  16,  //OT_MOTO_I_DRIVER4567
  17,  //OT_MOTO_I_DRIVER298N
  18,  //OT_MP3_INIT
  19,  //OT_MP3_DO
  20,  //OT_MP3_PLAYFOLDER
  21,  //OT_MP3_SETVOLUME
  24,  //OT_IR_INIT
  25,  //OT_IR_SEND
  26,  //OT_RETURN_IR
  27,  //OT_HX711_I
  28,  //OT_HX711_TEST
  29,  //OT_RETURN_HX711
  30,  //OT_DSTMAT_I
  31,  //OT_RETURN_DSTMAT
  32,  //OT_AXIS_I
  33,  //OT_RETURN_AXIS
  34,  //OT_SET_TIME,
  35,  //OT_RC_INIT
  36,  //OT_RC_SEND
  37,  //OT_RETRUN_RC
  38,  //OT_DHT_I
  39,  //OT_RETURN_DHTTEMP
  40,  //OT_RETURN_DHTHUMI
  41,  //OT_LEDSTRIP_I
  42,  //OT_LEDSTRIP_SET
  43,  //OT_SEGMENT_I
  44,  //OT_SEGMENT_BRIGHTNESS
  45,  //OT_SEGMENT_CLEAR
  46,  //OT_SEGMENT_DISPLAY
  47,  //OT_LEDMATRIX_I
  48,  //OT_LEDMATRIX_BRIGHTNESS
  49,  //OT_LEDMATRIX_CLEARSCREEN
  50,  //OT_LEDMATRIX_LIGHTAT
  51,  //OT_STEPMOTO_I
  52,  //OT_STEPMOTO_ENABLE
  53,  //OT_STEPMOTO_DIR
  54,  //OT_STEPMOTO_STEP
  55,  //OT_LCI2C_INIT
  56,  //OT_LCI2C_DO
  57,  //OT_LCI2C_SETCURSOR
  58,  //OT_LCI2C_SETBACKLIGHT
  59,  //OT_LCI2C_PRINT
  60,  //OT_LCI2C_PRINTBYTE
  61,  //OT_SERIAL_BEGIN,
  62,  //OT_MKSERIAL_SEND
  63,  //OT_RETURN_MKSERIAL
  64,  //OT_MKSERAL_CLEAR
  65,  //OT_DUST_I 
  66,  //OT_DUST_T
  67,  //OT_RETURN_DUST
  200  //OT_VERSION
};
//----------------------------------------------------------------------------
mkArduino::mkArduino()
{
  ardu = this;
  mNetID = 1314;  
  Objects = 0;

  Init();
}
//----------------------------------------------------------------------------
mkArduino::~mkArduino()
{
  if (Objects)
  {
    delete Objects;
  }
  Objects = 0;
}
//----------------------------------------------------------------------------
void mkArduino::Init()
{
  RecvStr = "";

  mSettedTimeMe = 0;
  mLastSendVersionTime = 0;
  mpCMD = 0;

  moto = 0;

  _SerialReceivedString_MK = "";

  Serial.begin(9600);

  digitalWrite(13, LOW);

  if (Objects)
  {
    delete Objects;
  }
  Objects = 0;

  Objects = new SimpleMap<String, mkObject* >([](String &a, String &b) -> int {
    if (a == b) return 0;      // a and b are equal
    else if (a > b) return 1;  // a is bigger than b
    else return -1;            // a is smaller than b
  });
}
//----------------------------------------------------------------------------
mkObject *mkArduino::GetObjectByNameUni(const String &nameuni)
{
  return Objects->get(nameuni);
}
//----------------------------------------------------------------------------
String mkArduino::_F2Str(float val)
{
  char str[16];
  dtostrf(val, 12, 4, str);
  return str;
}
//----------------------------------------------------------------------------
String mkArduino::_I2Str(int val)
{
  char str[25];
  itoa(val, str, 10); // 10 is decimal
  return str;
}
//----------------------------------------------------------------------------
void mkArduino::_SendNetID()
{
    char str[32];
    memset(str, 0, 32);
    itoa(mNetID, str, 10); // 10 is decimal

    unsigned char cmdCh = sOptTypeVal[OT_RETURN_NETID];
    char strCMDCh[32];
    memset(strCMDCh, 0, 32);
    itoa(cmdCh, strCMDCh, 10);

    String sendStr = String(strCMDCh) + " " + str;
    String lastCmdStr = "0000" + sendStr;
    Serial.println(lastCmdStr);
}
//----------------------------------------------------------------------------
void mkArduino::_SendVersion()
{
    unsigned char cmdCh = sOptTypeVal[OT_VERSION];
    char strCMDCh[32];
    memset(strCMDCh, 0, 32);
    itoa(cmdCh, strCMDCh, 10);
        
    String str = Useage;
    String sendStr = String(strCMDCh) + " " + str;
    String lastCmdStr = "0000" + sendStr;
    Serial.println(lastCmdStr);
}
//----------------------------------------------------------------------------

// serial
//----------------------------------------------------------------------------
void mkArduino::serialPrintln(String str){
  Serial.println(str);
}
//----------------------------------------------------------------------------
void mkArduino::clearSerialReceived(){
  RecvStr = "";
}
//----------------------------------------------------------------------------
String mkArduino::getSerialReceived(){
  return RecvStr;
}
//----------------------------------------------------------------------------
void mkArduino::serialBegin(unsigned long baudrate)
{
  Serial.begin(baudrate);  
}
//----------------------------------------------------------------------------
void mkArduino::serialSend_MK(String str)
{
     unsigned char cmdCh = sOptTypeVal[OT_MKSERIAL_SEND];
    char strCMDCh[32];
    memset(strCMDCh, 0, 32);
    itoa(cmdCh, strCMDCh, 10);

    String sendStr = String(strCMDCh) + " " + str;
    String lastCmdStr = "0000" + sendStr;
    Serial.println(lastCmdStr);
}
//----------------------------------------------------------------------------
String mkArduino::getSerialReceivedString_MK(){
  return _SerialReceivedString_MK;  
}
//----------------------------------------------------------------------------
void mkArduino::clearSerialReceivedString_MK()
{
  _SerialReceivedString_MK = "";
}
//----------------------------------------------------------------------------
void mkArduino::Tick()
{
  while (Serial.available())
  {
    char c = Serial.read();
    
    if ('\n' == c)
    {
      if (RecvStr.length() > 0)
      {
        // to compatile with PHOENIXEngine
        // 2 bytes for length and another 2 bytes for id
        String cmdStr = RecvStr.substring(4);
        OnCMD(mCmdParams, cmdStr);
      }
      RecvStr = "";
    }
    else if ('\r' == c){
      /*_*/
    }
    else
    {
      RecvStr += c;
    }
  }

  mTimer.update();

#if defined MK_SENDVERSION
  unsigned long tick = millis();       
  if (tick - mLastSendVersionTime >= 2000)
  {
      mLastSendVersionTime =tick;
      _SendVersion();
  } 
#endif

  if (Objects)
  {
    int sz = Objects->size();
    int i=0;
    for (i=0; i<sz; i++)
    {
      mkObject *obj = Objects->getData(i);
      if (obj)
      {
        obj->Update();
      }
    }
  }
}

//----------------------------------------------------------------------------
void mkArduino::_SetTimeMe()
{
  mSettedTimeMe = millis();
  if (0.0==mSettedTimeMe)
    mSettedTimeMe = 0.001;
}
//----------------------------------------------------------------------------
void mkArduino::_Delay(float seconds)
{
    long endTime = millis() + seconds * 1000;
    while(millis() < endTime)
      _EmptyLoop();
}
//----------------------------------------------------------------------------
void mkArduino::_EmptyLoop()
{
}
//----------------------------------------------------------------------------
void mkArduino::servoInit(const String &name, int pin)
{
#if defined MK_SERVO
  mkServo *obj = (mkServo*)GetObjectByNameUni(mkServo::sGenName(name));
  if (!obj)
  {
    obj = new mkServo();
    obj->OnCreate(name);
    Objects->put(obj->NameUni, obj);
  }
  obj->Init(pin);
#endif
}
//----------------------------------------------------------------------------
void mkArduino::servoWrite(const String &name, int val)
{
#if defined MK_SERVO
  mkServo *obj = (mkServo*)GetObjectByNameUni(mkServo::sGenName(name));
  if (obj)
  {
    obj->Write(val);
  }
#endif
}
//----------------------------------------------------------------------------
void mkArduino::temptureInit(const String &name, int pin)
{
#if defined MK_DHT
  mkDHT *obj = (mkDHT*)GetObjectByNameUni(mkDHT::sGenName(name));
  if (!obj)
  {
    obj = new mkDHT();
    obj->OnCreate(name);
    Objects->put(obj->NameUni, obj);
  }
  obj->Init(pin);
#endif
}
//----------------------------------------------------------------------------
float mkArduino::temptureGetTemperature(const String &name) const
{
#if defined MK_DHT
  mkDHT *obj = (mkDHT*)GetObjectByNameUni(mkDHT::sGenName(name));
  if (obj)
  {
    return obj->GetTemperature();
  }
#endif

  return 0.0f;
}
//----------------------------------------------------------------------------
float mkArduino::temptureGetHumidity(const String &name) const
{
#if defined MK_DHT
  mkDHT *obj = (mkDHT*)GetObjectByNameUni(mkDHT::sGenName(name));
  if (obj)
  {
    return obj->GetHumidity();
  }
#endif

  return 0.0f;
}
//----------------------------------------------------------------------------
void mkArduino::distInit(const String &name, int pinTrig, int pinEcho)
{
#if defined MK_DIST
  mkDist *obj = (mkDist*)GetObjectByNameUni(mkDist::sGenName(name));
  if (!obj)
  {
    obj = new mkDist();
    obj->OnCreate(name);
    Objects->put(obj->NameUni, obj);
  }
  obj->Init(pinTrig, pinEcho);
#endif
}
//----------------------------------------------------------------------------
float mkArduino::distMeasure(const String &name)
{
  #if defined MK_DIST
  mkDist *obj = (mkDist*)GetObjectByNameUni(mkDist::sGenName(name));
  if (obj)
  {
    obj->Test0();
    return obj->GetDist();
  }
#endif

  return 0.0f;
}
//----------------------------------------------------------------------------
void mkArduino::dustInit(const String &name, int pinLed, int pinSignal)
{
#if defined MK_DUST
  mkDust *obj = (mkDust*)GetObjectByNameUni(mkDust::sGenName(name));
  if (!obj)
  {
    obj = new mkDust();
    obj->OnCreate(name);
    Objects->put(obj->NameUni, obj);
  }
  obj->Init(pinLed, pinSignal);
#endif
}
//----------------------------------------------------------------------------
void mkArduino::dustTest(const String &name)
{
#if defined MK_DUST
  mkDust *obj = (mkDust*)GetObjectByNameUni(mkDust::sGenName(name));
  if (obj)
  {
    obj->Test0();
  }
#endif

  return 0.0f;
}
//----------------------------------------------------------------------------
float mkArduino::dustGetOutputV(const String &name) const
{
#if defined MK_DUST
  mkDust *obj = (mkDust*)GetObjectByNameUni(mkDust::sGenName(name));
  if (obj)
  {
    return obj->GetOutputV();
  }
#endif

  return 0.0f;
}
//----------------------------------------------------------------------------
float mkArduino::dustGetUgm3(const String &name) const
{
#if defined MK_DUST
  mkDust *obj = (mkDust*)GetObjectByNameUni(mkDust::sGenName(name));
  if (obj)
  {
    return obj->GetUgm3();
  }
#endif

  return 0.0f;
}
//----------------------------------------------------------------------------
float mkArduino::dustGetAqi(const String &name) const
{
#if defined MK_DUST
  mkDust *obj = (mkDust*)GetObjectByNameUni(mkDust::sGenName(name));
  if (obj)
  {
    return obj->GetAqi();
  }
#endif

  return 0.0f;
}
//----------------------------------------------------------------------------
int mkArduino::dustGetGrade(const String &name) const
{
#if defined MK_DUST
  mkDust *obj = (mkDust*)GetObjectByNameUni(mkDust::sGenName(name));
  if (obj)
  {
    return obj->GetGrade();
  }
#endif

  return 0.0f;
}
//----------------------------------------------------------------------------
String mkArduino::dustGetGradeInfo(const String &name) const
{
#if defined MK_DUST
  mkDust *obj = (mkDust*)GetObjectByNameUni(mkDust::sGenName(name));
  if (obj)
  {
    return obj->GetGradeInfo();
  }
#endif

  return "";
}
//----------------------------------------------------------------------------
void mkArduino::irReceiverInit(const String &name, int pin)
{
#if defined MK_IR
  mkIR *obj = (mkIR*)GetObjectByNameUni(mkIR::sGenName(name));
  if (!obj)
  {
    obj = new mkIR();
    obj->OnCreate(name);
    Objects->put(obj->NameUni, obj);
  }
  obj->Init(pin);
#endif
}
//----------------------------------------------------------------------------
float mkArduino::irGetReceivedValue(const String &name) const
{
#if defined MK_IR
  mkIR *obj = (mkIR*)GetObjectByNameUni(mkIR::sGenName(name));
  if (obj)
  {
    return obj->GetReceivedValue();
  }
#endif

  return 0.0f;
}
//----------------------------------------------------------------------------
void mkArduino::irSend(const String &name, int val)
{
#if defined MK_IR
  mkIR *obj = (mkIR*)GetObjectByNameUni(mkIR::sGenName(name));
  if (obj)
  {
    obj->Send(val);
  }
#endif
}
//----------------------------------------------------------------------------
void mkArduino::ledmatrixInit(const String &name, int sckPin, int dinPin)
{
#if defined MK_LEDMATRIX
  mkLEDMatrix *obj = (mkLEDMatrix*)GetObjectByNameUni(mkLEDMatrix::sGenName(name));
  if (!obj)
  {
    obj = new mkLEDMatrix();
    obj->OnCreate(name);
    Objects->put(obj->NameUni, obj);
  }
  obj->Init(sckPin, dinPin);
#endif
}
//----------------------------------------------------------------------------
void mkArduino::ledmatrixSetBrightness(const String &name, int brightness)
{
#if defined MK_LEDMATRIX
  mkLEDMatrix *obj = (mkLEDMatrix*)GetObjectByNameUni(mkLEDMatrix::sGenName(name));
  if (obj)
  {
    obj->SetBrightness(brightness);
  }
#endif
}
//----------------------------------------------------------------------------
void mkArduino::ledmatrixClearScreen()
{
#if defined MK_LEDMATRIX
  mkLEDMatrix *obj = (mkLEDMatrix*)GetObjectByNameUni(mkLEDMatrix::sGenName(name));
  if (obj)
  {
    obj->ClearScreen();
  }
#endif
}
//----------------------------------------------------------------------------
void mkArduino::ledmatrixSetLightPos(const String &name, int8_t x, int8_t y, 
int width, bool onOff)
{
#if defined MK_LEDMATRIX
  mkLEDMatrix *obj = (mkLEDMatrix*)GetObjectByNameUni(mkLEDMatrix::sGenName(name));
  if (obj)
  {
    obj->SetLightPos(x, y, width, onOff);
  }
#endif
}
//----------------------------------------------------------------------------
void mkArduino::motoInit4567(const String &name)
{
#if defined MK_MOTO
  mkMoto *obj = (mkMoto*)GetObjectByNameUni(mkMoto::sGenName(name));
  if (!obj)
  {
    obj = new mkMoto();
    obj->OnCreate(name);
    Objects->put(obj->NameUni, obj);
  }
  obj->Init4567();
#endif
}
//----------------------------------------------------------------------------
void mkArduino::motoInit10111213(const String &name)
{
#if defined MK_MOTO
  mkMoto *obj = (mkMoto*)GetObjectByNameUni(mkMoto::sGenName(name));
  if (!obj)
  {
    obj = new mkMoto();
    obj->OnCreate(name);
    Objects->put(obj->NameUni, obj);
  }
  obj->Init10111213();
#endif
}
//----------------------------------------------------------------------------
void mkArduino::motoInit298N(const String &name, int pinL, int pinL1,
 int pinLS, int pinR, int pinR1, int pinRS)
{
#if defined MK_MOTO
  mkMoto *obj = (mkMoto*)GetObjectByNameUni(mkMoto::sGenName(name));
  if (!obj)
  {
    obj = new mkMoto();
    obj->OnCreate(name);
    Objects->put(obj->NameUni, obj);
  }
  obj->Init298N(pinL, pinL1, pinLS, pinR, pinR1, pinRS);
#endif
}
//----------------------------------------------------------------------------
void mkArduino::motoPidInit(const String &name, int encorderLA,
 int encorderLB, int encorderRA, int encorderRB)
{
#if defined MK_MOTO
  mkMoto *obj = (mkMoto*)GetObjectByNameUni(mkMoto::sGenName(name));
  if (obj)
  {
    obj->PidInit(encorderLA, encorderLB, encorderRA, encorderRB);
  }
#endif
}
//----------------------------------------------------------------------------
void mkArduino::motoPidShutdown(const String &name)
{
#if defined MK_MOTO
  mkMoto *obj = (mkMoto*)GetObjectByNameUni(mkMoto::sGenName(name));
  if (obj)
  {
    obj->PidShutdown();
  }
#endif
}
//----------------------------------------------------------------------------
void mkArduino::motoRun(const String &name, int leftright, int dir, int spd)
{
#if defined MK_MOTO
  mkMoto *obj = (mkMoto*)GetObjectByNameUni(mkMoto::sGenName(name));
  if (obj)
  {
    obj->Run(leftright, dir, spd);
  }
#endif
}
//----------------------------------------------------------------------------
void mkArduino::motoRunLeftRight(const String &name, int dir, int spd)
{
#if defined MK_MOTO
  mkMoto *obj = (mkMoto*)GetObjectByNameUni(mkMoto::sGenName(name));
  if (obj)
  {
    obj->RunLeftRight(dir, spd);
  }
#endif
}
//----------------------------------------------------------------------------
void mkArduino::motoRunLeft(const String &name, int dir, int spd)
{
#if defined MK_MOTO
  mkMoto *obj = (mkMoto*)GetObjectByNameUni(mkMoto::sGenName(name));
  if (obj)
  {
    obj->RunLeft(dir, spd);
  }
#endif
}
//----------------------------------------------------------------------------
void mkArduino::motoRunRight(const String &name, int dir, int spd)
{
#if defined MK_MOTO
  mkMoto *obj = (mkMoto*)GetObjectByNameUni(mkMoto::sGenName(name));
  if (obj)
  {
    obj->RunRight(dir, spd);
  }
#endif
}
//----------------------------------------------------------------------------
void mkArduino::motoStop(const String &name)
{
#if defined MK_MOTO
  mkMoto *obj = (mkMoto*)GetObjectByNameUni(mkMoto::sGenName(name));
  if (obj)
  {
    obj->Stop();
  }
#endif
}
//----------------------------------------------------------------------------
void mkArduino::mp3Init(const String &name, int pinR, int pinT)
{
#if defined MK_MP3
  mkMP3 *obj = (mkMP3*)GetObjectByNameUni(mkMP3::sGenName(name));
  if (!obj)
  {
    obj = new mkMP3();
    obj->OnCreate(name);
    Objects->put(obj->NameUni, obj);
  }
  obj->Init(pinR, pinT);
#endif
}
//----------------------------------------------------------------------------
void mkArduino::mp3Do(const String &name, int type)
{
#if defined MK_MP3
  mkMP3 *obj = (mkMP3*)GetObjectByNameUni(mkMP3::sGenName(name));
  if (obj)
  {
    obj->Do(type);
  }
#endif
}
//----------------------------------------------------------------------------
void mkArduino::mp3FolderPlay(const String &name, int folder, int index)
{
#if defined MK_MP3
  mkMP3 *obj = (mkMoto*)GetObjectByNameUni(mkMP3::sGenName(name));
  if (obj)
  {
    obj->FolderPlay(folder, index);
  }
#endif
}
//----------------------------------------------------------------------------
void mkArduino::mp3SetVolume(const String &name, int val)
{
#if defined MK_MP3
  mkMP3 *obj = (mkMoto*)GetObjectByNameUni(mkMP3::sGenName(name));
  if (obj)
  {
    obj->SetVolume(val);
  }
#endif
}
//----------------------------------------------------------------------------
void mkArduino::rgbledInit(const String &name, int pin, int num)
{
#if defined MK_RGBLED
  mkRGBLed *obj = (mkRGBLed*)GetObjectByNameUni(mkRGBLed::sGenName(name));
  if (!obj)
  {
    obj = new mkRGBLed();
    obj->OnCreate(name);
    Objects->put(obj->NameUni, obj);
  }
  obj->Init(pin, num);
#endif
}
//----------------------------------------------------------------------------
void mkArduino::rgbledSetColor(const String &name, int index, int r, int g, 
int b)
{
#if defined MK_RGBLED
  mkRGBLed *obj = (mkRGBLed*)GetObjectByNameUni(mkRGBLed::sGenName(name));
  if (obj)
  {
    obj->SetColor(index, r, g, b);
  }
#endif
}
//----------------------------------------------------------------------------
void mkArduino::lci2cInit(const String &name, int addr, int numCols, 
int numRows)
{
#if defined MK_SCREEN_I2C
  mkScreenI2C *obj = (mkScreenI2C*)GetObjectByNameUni(mkScreenI2C::sGenName(name));
  if (!obj)
  {
    obj = new mkScreenI2C();
    obj->OnCreate(name);
    Objects->put(obj->NameUni, obj);
  }
  obj->Init(addr, numCols, numRows);
#endif
}
//----------------------------------------------------------------------------
void mkArduino::lci2cDo(const String &name, int doType)
{
#if defined MK_SCREEN_I2C
  mkScreenI2C *obj = (mkScreenI2C*)GetObjectByNameUni(mkScreenI2C::sGenName(name));
  if (obj)
  {
    obj->Do(doType);
  }
#endif
}
//----------------------------------------------------------------------------
void mkArduino::lci2cSetCursor(const String &name, int col, int row)
{
#if defined MK_SCREEN_I2C
  mkScreenI2C *obj = (mkScreenI2C*)GetObjectByNameUni(mkScreenI2C::sGenName(name));
  if (obj)
  {
    obj->SetCursor(col, row);
  }
#endif
}
//----------------------------------------------------------------------------
void mkArduino::lci2cSetBackLight(const String &name, int val)
{
#if defined MK_SCREEN_I2C
  mkScreenI2C *obj = (mkScreenI2C*)GetObjectByNameUni(mkScreenI2C::sGenName(name));
  if (obj)
  {
    obj->SetBackLight(val);
  }
#endif
}
//----------------------------------------------------------------------------
void mkArduino::lci2cPrint(const String &name, const String &val)
{
#if defined MK_SCREEN_I2C
  mkScreenI2C *obj = (mkScreenI2C*)GetObjectByNameUni(mkScreenI2C::sGenName(name));
  if (obj)
  {
    obj->Print(val);
  }
#endif
}
//----------------------------------------------------------------------------
void mkArduino::lci2cPrintByte(const String &name, int selfCreateCharIndex)
{
#if defined MK_SCREEN_I2C
  mkScreenI2C *obj = (mkScreenI2C*)GetObjectByNameUni(mkScreenI2C::sGenName(name));
  if (obj)
  {
    obj->PrintByte(selfCreateCharIndex);
  }
#endif
}
//----------------------------------------------------------------------------
void mkArduino::segmentInit(const String &name, int clkPin, int dataPin)
{
#if defined MK_SEGMENT
  mkSegment *obj = (mkSegment*)GetObjectByNameUni(mkSegment::sGenName(name));
  if (!obj)
  {
    obj = new mkSegment();
    obj->OnCreate(name);
    Objects->put(obj->NameUni, obj);
  }
  obj->Init(clkPin, dataPin);
#endif
}
//----------------------------------------------------------------------------
void mkArduino::segmentSetBrightness(const String &name, int brightness)
{
#if defined MK_SEGMENT
  mkSegment *obj = (mkSegment*)GetObjectByNameUni(mkSegment::sGenName(name));
  if (obj)
  {
    obj->SetBrightness(brightness);
  }
#endif
}
//----------------------------------------------------------------------------
void mkArduino::segmentClear(const String &name)
{
#if defined MK_SEGMENT
  mkSegment *obj = (mkSegment*)GetObjectByNameUni(mkSegment::sGenName(name));
  if (obj)
  {
    obj->Clear();
  }
#endif
}
//----------------------------------------------------------------------------
void mkArduino::segmentDisplayInt(const String &name, int val)
{
#if defined MK_SEGMENT
  mkSegment *obj = (mkSegment*)GetObjectByNameUni(mkSegment::sGenName(name));
  if (obj)
  {
    obj->DisplayInt(val);
  }
#endif
}
//----------------------------------------------------------------------------
void mkArduino::segmentDisplayFloat(const String &name, float val)
{
  #if defined MK_SEGMENT
  mkSegment *obj = (mkSegment*)GetObjectByNameUni(mkSegment::sGenName(name));
  if (obj)
  {
    obj->DisplayFloat(val);
  }
#endif
}
//----------------------------------------------------------------------------