// mkservo.h

#ifndef MKSERVO_H
#define MKSERVO_H

#include "mkarduinodef.h"

#if defined MK_SERVO

#include "mkobject.h"
#include <Servo.h>

class mkServo : public mkObject
{
public:
    mkServo();
    virtual ~mkServo();

    static String sGenName(const String &t) { return "12_" + t; }
    virtual String GenName(const String &t) { return "12_" + t; }

    static mkObject *Create(unsigned char cmdCH, String *cmdParams);

    virtual void OnCMD(unsigned char cmdCH, String *cmdParams);

    void Init(int pin);
    void Write(int val);

private:
    Servo mServo;
};

#endif

#endif
