// mkrcswitch.h

#ifndef MKRCSWITCH_H
#define MKRCSWITCH_H

#include "mkarduinodef.h"

#if defined MK_RCSWITCH

#include "mkobject.h"
#include "_mkrcswitch.h"

class mkRCSwitch : public mkObject
{
public:
    mkRCSwitch();
    virtual ~mkRCSwitch();

    static String sGenName(const String &t) { return "8_" + t; }
    virtual String GenName(const String &t) { return "8_" + t; }

    static mkObject *Create(unsigned char cmdCH, String *cmdParams);

    virtual void Update();
    virtual void OnCMD(unsigned char cmdCH, String *cmdParams);

    void InitRCSwitchReceive(int pinTimerIndex);
    void RCInit(int pin);
    void RCSend(long val);

private:
    RCSwitch mRCSwitch;
};

#endif

#endif
