// mkledmatrix.cpp

#include "mkledmatrix.h"

#if defined MK_LEDMATRIX

#include "mkarduino.h"

//----------------------------------------------------------------------------
mkLEDMatrix::mkLEDMatrix()
{
}
//----------------------------------------------------------------------------
mkLEDMatrix::~mkLEDMatrix()
{
}
//----------------------------------------------------------------------------
mkObject *mkLEDMatrix::Create(unsigned char cmdCH, String *cmdParams)
{
    if (mkArduino::sOptTypeVal[OT_LEDMATRIX_I]==cmdCH)
    {
        String name = cmdParams[1];
        if (!mkArduino::ardu->GetObjectByNameUni(sGenName(name)))
        {
            mkObject *obj = new mkLEDMatrix();
            obj->OnCreate(name, cmdCH, cmdParams);
            return obj;
        }
    }

    return 0;
}
//----------------------------------------------------------------------------
void mkLEDMatrix::OnCMD(unsigned char cmdCH, String *cmdParams)
{
    if (mkArduino::sOptTypeVal[OT_LEDMATRIX_I]==cmdCH)
    {
      int pinClk = mkArduino::_Str2Pin(cmdParams[2]);
      int pinData = mkArduino::_Str2Pin(cmdParams[3]);
      LEDMatrixInit(pinClk, pinData);
    }
    else if (mkArduino::sOptTypeVal[OT_LEDMATRIX_BRIGHTNESS]==cmdCH)
    {
      int val = mkArduino::_Str2Int(cmdParams[2]);
      LEDMatrixSetBrightness(val);
    }
    else if (mkArduino::sOptTypeVal[OT_LEDMATRIX_CLEARSCREEN]==cmdCH)
    {
      LEDMatrixClearScreen();
    }
    else if (mkArduino::sOptTypeVal[OT_LEDMATRIX_LIGHTAT]==cmdCH)
    {
      int x = mkArduino::_Str2Int(cmdParams[2]);
      int y = mkArduino::_Str2Int(cmdParams[3]);
      int width = mkArduino::_Str2Int(cmdParams[4]);
      bool onOff = mkArduino::_Str2Bool(cmdParams[5]);

      LEDMatrixLightPos(x,y,width,onOff);
    }
}
//----------------------------------------------------------------------------
void mkLEDMatrix::Init(int sckPin, int dinPin)
{
    mLEDMatrix = LEDMatrix(sckPin, dinPin);
    mLEDMatrix.setColorIndex(1);
}
//----------------------------------------------------------------------------
void mkLEDMatrix::SetBrightness(int brightness)
{
    mLEDMatrix.setBrightness(brightness);
}
//----------------------------------------------------------------------------
void mkLEDMatrix::ClearScreen()
{
    mLEDMatrix.clearScreen();
}
//----------------------------------------------------------------------------
void mkLEDMatrix::SetColorIndex(int iColor_Number)
{
    mLEDMatrix.setColorIndex(iColor_Number);
}
//----------------------------------------------------------------------------
void mkLEDMatrix::DrawBitmap(int8_t x, int8_t y, uint8_t bitmap_Width, 
    uint8_t *bitmap)
{
   mLEDMatrix.drawBitmap(x, y, bitmap_Width, bitmap);
}
//----------------------------------------------------------------------------
void mkLEDMatrix::SetLightPos(int8_t x, int8_t y, int width, bool onOff)
{
    if (width > 16)
        width = 16;
        
    if (onOff)
    {
        unsigned char buf[16]={128,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0};
        for (int i=0; i<width; i++)
        {
            buf[i] = 128;
        }

        mLEDMatrix.drawBitmap(x,y,16,buf);
    }
    else
    {
        unsigned char buf[16]={0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0};
        for (int i=0; i<width; i++)
        {
            buf[i] = 0;
        }
        mLEDMatrix.drawBitmap(x,y,16,buf);
    }
}
//----------------------------------------------------------------------------
void mkLEDMatrix::DrawStr(int16_t x_position, int8_t y_position,
     const char *str)
{
    mLEDMatrix.drawStr(x_position, y_position, str);
}
//----------------------------------------------------------------------------
void mkLEDMatrix::ShowClock(uint8_t hour, uint8_t minute,
 bool isPointOn)
{
    mLEDMatrix.showClock(hour, minute, isPointOn);
}
//----------------------------------------------------------------------------
void mkLEDMatrix::ShowNum(float value,uint8_t val)
{
    mLEDMatrix.showNum(value, val);
}
//----------------------------------------------------------------------------
#endif