// mkir.h

#ifndef MKIR_H
#define MKIR_H

#include "mkarduinodef.h"

#if defined MK_IR

#include "mkobject.h"
#include "_mkirremote.h"

class mkIR : public mkObject
{
public:
    mkIR();
    virtual ~mkIR();

    static String sGenName(const String &t) { return "4_" + t; }
    virtual String GenName(const String &t) { return "4_" + t; }

    static mkObject *Create(unsigned char cmdCH, String *cmdParams);

    virtual void OnSendBack();
    virtual void Update();
    virtual void OnCMD(unsigned char cmdCH, String *cmdParams);

    void Init(int pinR);
    void Send(int val);
    int GetReceivedValue();

private:
    void _SendbackIRRecv(int val);

    IRrecv *mIRrecv;
    IRsend mIRsend;
    int mIRRecvValue;
};

#endif

#endif
