// mkrgbled.h

#ifndef MKRGBLED_H
#define MKRGBLED_H

#include "mkarduinodef.h"

#if defined MK_RGBLED

#include "mkobject.h"
#include "_mkws2812.h"

class mkRGBLed : public mkObject
{
public:
    mkRGBLed();
    virtual ~mkRGBLed();

    static String sGenName(const String &t) { return "9_" + t; }
    virtual String GenName(const String &t) { return "9_" + t; }

    static mkObject *Create(unsigned char cmdCH, String *cmdParams);

    virtual void Update();
    virtual void OnCMD(unsigned char cmdCH, String *cmdParams);

    void Init(int pin, int num);
    void SetColor(int index, int r, int g, int b);

private:
    WS2812 mWS2812;
};

#endif

#endif
