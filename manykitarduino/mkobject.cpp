// mkobject.cpp

#include "mkobject.h"

//----------------------------------------------------------------------------
mkObject::mkObject() 
{
    mSendBackTime = 33; 
    mSendBackLastTime = 0;
}
//----------------------------------------------------------------------------
mkObject::~mkObject() 
{    
}
//----------------------------------------------------------------------------
String mkObject::GenName(const String &t) 
{
     return "";
}
//----------------------------------------------------------------------------
void mkObject::OnCreate(const String &name)
{
    Name = name;
    NameUni = GenName(name);
}
//----------------------------------------------------------------------------    
void mkObject::OnCreate(const String &name, unsigned char cmdCH, String *cmdParams)
{
    Name = name;
    NameUni = GenName(name);
    OnCMD(cmdCH, cmdParams);
}
//----------------------------------------------------------------------------
void mkObject::SetSendBackTime(int miliSeconds)
{
    mSendBackTime = miliSeconds;
}
//----------------------------------------------------------------------------
void mkObject::Update()
{
    if(millis() - mSendBackLastTime > mSendBackTime)
    {
        mSendBackLastTime = millis();
        OnSendBack();
    }
}
//----------------------------------------------------------------------------
void mkObject::OnSendBack()
{
}
//----------------------------------------------------------------------------
void mkObject::OnCMD(unsigned char cmdCH, String *cmdParams)
{
}
//----------------------------------------------------------------------------