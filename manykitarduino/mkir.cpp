// mkir.cpp

#include "mkir.h"

#if defined MK_IR

#include "mkarduino.h"

//----------------------------------------------------------------------------
mkIR::mkIR()
{
    mIRrecv = 0;
}
//----------------------------------------------------------------------------
mkIR::~mkIR()
{
    if (mIRrecv)
    {
        delete mIRrecv;
    }
    
    mIRrecv = 0;
    mIRRecvValue = 0;
}
//----------------------------------------------------------------------------
mkObject *mkIR::Create(unsigned char cmdCH, String *cmdParams)
{
    if (mkArduino::sOptTypeVal[OT_IR_INIT]==cmdCH)
    {
        String name = cmdParams[1];
        if (!mkArduino::ardu->GetObjectByNameUni(sGenName(name)))
        {
            mkObject *obj = new mkIR();
            obj->OnCreate(name, cmdCH, cmdParams);
            return obj;
        }
    }

    return 0;
}
//----------------------------------------------------------------------------
void mkIR::Update()
{
    mkObject::Update();
    
    decode_results iresultes;
    if (mIRrecv)
    {
        if (mIRrecv->decode(&iresultes))
        {
            mIRRecvValue = iresultes.value;
            mIRrecv->resume();
        }
        else
        {
            mIRRecvValue = 0;
        }
    }
}
//----------------------------------------------------------------------------
void mkIR::OnCMD(unsigned char cmdCH, String *cmdParams)
{
    if (mkArduino::sOptTypeVal[OT_IR_INIT]==cmdCH)
    {
      int pinR = mkArduino::_Str2Pin(cmdParams[2]);
      Init(pinR);      
    }
    else if (mkArduino::sOptTypeVal[OT_IR_SEND]==cmdCH)
    {
      int val = mkArduino::_Str2Int(cmdParams[2]);
      Send(val);
    }
}
//----------------------------------------------------------------------------
void mkIR::Init(int pinR)
{
    if (mIRrecv)
    {
        delete mIRrecv;
        mIRrecv = 0;
    }

    pinMode(pinR, INPUT);
    mIRrecv = new IRrecv(pinR);
    mIRrecv->enableIRIn();
}
//----------------------------------------------------------------------------
void mkIR::Send(int val)
{
    mIRsend.sendSony(val, 32);

    if (mIRrecv)
      mIRrecv->enableIRIn();
}
//----------------------------------------------------------------------------
int mkIR::GetReceivedValue()
{
    return mIRRecvValue;
}
//----------------------------------------------------------------------------
void mkIR::OnSendBack()
{
    _SendbackIRRecv(mIRRecvValue);
}
//----------------------------------------------------------------------------
void mkIR::_SendbackIRRecv(int val)
{
   unsigned char cmdCh = mkArduino::sOptTypeVal[OT_RETURN_IR];
    char strCMDCh[32];
    memset(strCMDCh, 0, 32);
    itoa(cmdCh, strCMDCh, 10);

    Serial.print("0000");
    Serial.print(String(strCMDCh));
    Serial.print(" ");
    Serial.print(Name);
    Serial.print(" ");
    Serial.println(val);
}
//----------------------------------------------------------------------------

#endif