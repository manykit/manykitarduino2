// mkdist.cpp

#include "mkdist.h"

#if defined MK_DIST

#include "mkarduino.h"

//----------------------------------------------------------------------------
mkDist::mkDist()
{
    mPinDistTrigger = 0;
    mPinDistEcho = 0;
    mDist = 0.0f;
    mDistCheckLastTime = 0;
}
//----------------------------------------------------------------------------
mkDist::~mkDist()
{
}
//----------------------------------------------------------------------------
mkObject *mkDist::Create(unsigned char cmdCH, String *cmdParams)
{
    if (mkArduino::sOptTypeVal[OT_DST_I]==cmdCH)
    {
        String name = cmdParams[1];
        if (!mkArduino::ardu->GetObjectByNameUni(sGenName(name)))
        {
            mkObject *obj = new mkDist();
            obj->OnCreate(name, cmdCH, cmdParams);
            return obj;
        }
    }

    return 0;
}
//----------------------------------------------------------------------------
void mkDist::OnSendBack()
{    
      unsigned char cmdCh = mkArduino::sOptTypeVal[OT_RETURN_DIST];
      char strCMDCh[32];
      memset(strCMDCh, 0, 32);
      itoa(cmdCh, strCMDCh, 10);
      
      Serial.print("0000");
      Serial.print(String(strCMDCh)); 
      Serial.print(" ");
      Serial.print(Name);
      Serial.print(" ");
      Serial.println(mDist);
}
//----------------------------------------------------------------------------
void mkDist::OnCMD(unsigned char cmdCH, String *cmdParams)
{
    if (mkArduino::sOptTypeVal[OT_DST_I]==cmdCH)
    {
      int pinTrigger = mkArduino::_Str2Pin(cmdParams[2]);
      int pinEcho = mkArduino::_Str2Pin(cmdParams[3]);
      Init(pinTrigger, pinEcho);
    }
    else if (mkArduino::sOptTypeVal[OT_DST_T]==cmdCH)
    {
        Test0();
    }
}
//----------------------------------------------------------------------------
void mkDist::Init(int pinTrig, int pinEcho)
{
    mPinDistTrigger = pinTrig;
    mPinDistEcho = pinEcho;
    pinMode(mPinDistTrigger, OUTPUT);
    pinMode(mPinDistEcho, INPUT);

    mDist = 0.0f;
    mDistCheckLastTime = 0;
}
//----------------------------------------------------------------------------
void mkDist::Test0()
{
    if(millis() - mDistCheckLastTime > 25)
    {
        mDistCheckLastTime = millis();
        Test(); 
    }
}
//----------------------------------------------------------------------------
void mkDist::Test()
{
    digitalWrite(mPinDistTrigger, LOW);
    delayMicroseconds(2);
    digitalWrite(mPinDistTrigger, HIGH);
    delayMicroseconds(10);
    digitalWrite(mPinDistTrigger, LOW);

    float dist = pulseIn(mPinDistEcho, HIGH) / 58.0;
    dist = (int(dist * 100.0)) / 100.0;
    if (2 <= dist && dist <= 400)
    {
        mDist = dist;
    }
}
//----------------------------------------------------------------------------
float mkDist::GetDist() const
{
    return mDist;
}
//----------------------------------------------------------------------------

#endif