// mkdist.cpp

#include "mkdust.h"

#if defined MK_DUST

#include "mkarduino.h"
#include "_mkgp2y1010au0f.h"

//----------------------------------------------------------------------------
const int DELAY_BEFORE_SAMPLING = 280; //采样前等待时间
const int DELAY_AFTER_SAMPLING = 40; //采样后等待时间
const int DELAY_LED_OFF = 9680; //间隔时间
//----------------------------------------------------------------------------
mkDust::mkDust()
{
    mGP2Y1010AU0F = 0;
    mDustCheckLastTime = 0;
}
//----------------------------------------------------------------------------
mkDust::~mkDust()
{
    if (mGP2Y1010AU0F)
    {
        delete(mGP2Y1010AU0F);
    }
}
//----------------------------------------------------------------------------
mkObject *mkDust::Create(unsigned char cmdCH, String *cmdParams)
{
    if (mkArduino::sOptTypeVal[OT_DUST_I]==cmdCH)
    {
        String name = cmdParams[1];
        if (!mkArduino::ardu->GetObjectByNameUni(sGenName(name)))
        {
            mkObject *obj = new mkDust();
            obj->OnCreate(name, cmdCH, cmdParams);
            return obj;
        }
    }

    return 0;
}
//----------------------------------------------------------------------------
void mkDust::OnSendBack()
{
    unsigned char cmdCh = mkArduino::sOptTypeVal[OT_RETURN_DUST];
    char strCMDCh[32];
    memset(strCMDCh, 0, 32);
    itoa(cmdCh, strCMDCh, 10);

    Serial.print("0000");
    Serial.print(String(strCMDCh)); 
    Serial.print(" ");
    Serial.print(Name);
    Serial.print(" ");
    Serial.print(mOutputV);
    Serial.print(" ");
    Serial.print(mUgm3);
    Serial.print(" ");
    Serial.print(mAqi);
    Serial.print(" ");
    Serial.print(mGrade);
    Serial.print(" ");
    Serial.println(mGradeInfo);
}
//----------------------------------------------------------------------------
void mkDust::OnCMD(unsigned char cmdCH, String *cmdParams)
{
    if (mkArduino::sOptTypeVal[OT_DUST_I]==cmdCH)
    {
        int pinLed = mkArduino::_Str2Pin(cmdParams[2]);
        int pinOutput = mkArduino::_Str2Pin(cmdParams[3]);
        Init(pinLed, pinOutput);
    }
    else if (mkArduino::sOptTypeVal[OT_DUST_T]==cmdCH)
    {
        Test0();
    }
}
//----------------------------------------------------------------------------
void mkDust::Init(int pinLed, int pinOutput)
{
    if (mGP2Y1010AU0F)
    {
        delete(mGP2Y1010AU0F);
    }
    mGP2Y1010AU0F = 0;

    mGP2Y1010AU0F = new GP2Y1010AU0F(pinLed, pinOutput);
}
//----------------------------------------------------------------------------
void mkDust::Test0()
{
    if(millis() - mDustCheckLastTime > 20)
    {
        mDustCheckLastTime = millis();
        Test();
    }
}
//----------------------------------------------------------------------------
void mkDust::Test()
{
    if (mGP2Y1010AU0F)
    {
        mOutputV = mGP2Y1010AU0F->getOutputV();

        mUgm3 = mGP2Y1010AU0F->getDustDensity(mOutputV); //计算灰尘浓度
        mAqi = mGP2Y1010AU0F->getAQI(mUgm3); //计算aqi
        mGrade = mGP2Y1010AU0F->getAQI(mAqi);

        switch (mGrade) {
            case GRADE_PERFECT:
            mGradeInfo = String("PERFECT");
            break;
            case GRADE_GOOD:
            mGradeInfo = String("GOOD");
            break;
            case GRADE_POLLUTED_MILD:
            mGradeInfo = String("POLLUTED_MILD");
            break;
            case GRADE_POLLUTED_MEDIUM:
            mGradeInfo = String("POLLUTED_MEDIUM");
            break;
            case GRADE_POLLUTED_HEAVY:
            mGradeInfo = String("POLLUTED_HEAVY");
            break;
            case GRADE_POLLUTED_SEVERE:
            mGradeInfo = String("POLLUTED_SEVERE");
            break;
            default:
            mGradeInfo = String("POLLUTED_SEVERE");
        }
    }
}
//----------------------------------------------------------------------------
float mkDust::GetOutputV() const
{
    return mOutputV;
}
//----------------------------------------------------------------------------
float mkDust::GetUgm3() const
{
    return mUgm3;
}
//----------------------------------------------------------------------------
float mkDust::GetAqi() const
{
    return mAqi;
}
//----------------------------------------------------------------------------
int mkDust::GetGrade() const
{
    return mGrade;
}
//----------------------------------------------------------------------------
const String &mkDust::GetGradeInfo() const
{
    return mGradeInfo;
}
//----------------------------------------------------------------------------

#endif