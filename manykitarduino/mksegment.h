// mkSegment.h

#ifndef MKSEGMENT_H
#define MKSEGMENT_H

#include "mkarduinodef.h"

#if defined MK_SEGMENT

#include "mkobject.h"
#include "_mksegmentdisplay.h"

class mkSegment : public mkObject
{
public:
    mkSegment();
    virtual ~mkSegment();

    static String sGenName(const String &t) { return "11_" + t; }
    virtual String GenName(const String &t) { return "11_" + t; }

    static mkObject *Create(unsigned char cmdCH, String *cmdParams);

    virtual void Update();
    virtual void OnCMD(unsigned char cmdCH, String *cmdParams);

    void Init(int clkPin, int dataPin);
    void SetBrightness(int brightness);
    void Clear();
    void DisplayInt(int val);
    void DisplayFloat(float val);

private:
    SegmentDisplay mSegmentDisplay;
};

#endif

#endif
