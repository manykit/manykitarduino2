// mkarduinocmd.cpp

#include "mkArduino.h"

#include "mkdht.h"
#include "mkdist.h"
#include "mkhx711.h"
#include "mkir.h"
#include "mkledmatrix.h"
#include "mkmoto.h"
#include "mkmp3.h"
#include "mkrcswitch.h"
#include "mkrgbled.h"
#include "mkscreeni2c.h"
#include "mksegment.h"
#include "mkservo.h"
#include "mkdust.h"

//----------------------------------------------------------------------------
void mkArduino::OnCMD(String *cmdParams, String &cmdStr)
{
  int i=0;
  for (; i<NumCMDParams; i++)
  {
    cmdParams[i] = "";
  }

  String strs = cmdStr;
  int cmdIndexTemp = 0;
  char *pCMDParam = strtok((char *)strs.c_str(), " ");
  while (pCMDParam)
  {
    cmdParams[cmdIndexTemp] = String(pCMDParam);
    cmdIndexTemp++;
    pCMDParam = strtok(NULL, " ");

    if (cmdIndexTemp > NumCMDParams)
      break;
  }

  if (cmdIndexTemp > 0)
  {
    unsigned char cmdCH = atoi(cmdParams[0].c_str());
    if (sOptTypeVal[OT_TOGET_NETID] == cmdCH)
    {
      _SendNetID();
    }
    else if (sOptTypeVal[OT_SET_TIME] == cmdCH)
    {
      _SetTimeMe();
    }
    else if (sOptTypeVal[OT_PM] == cmdCH)
    {
      int pin = _Str2Pin(cmdParams[1]);
      int val = _Str2IO(cmdParams[2]);
      pinMode(pin, val);     
    }
    else if(sOptTypeVal[OT_DW]==cmdCH) 
    {
      int pin = _Str2Pin(cmdParams[1]);
      bool isHigh = _Str2Bool(cmdParams[2]);
      digitalWrite(pin, isHigh?HIGH:LOW);
    }
    else if (sOptTypeVal[OT_AW]==cmdCH)
    {
      int pin = _Str2Pin(cmdParams[1]);
      int val = _Str2Int(cmdParams[2]);
      analogWrite(pin, val);
    }
    else if (sOptTypeVal[OT_RETURN_DR]==cmdCH)
    {
      int pinI = _Str2PinIndex(cmdParams[1]);
      int pin = _Str2Pin(cmdParams[1]);
      int val = digitalRead(pin);
      
      unsigned char cmdCh = sOptTypeVal[OT_RETURN_DR];
      char strCMDCh[32];
      memset(strCMDCh, 0, 32);
      itoa(cmdCh, strCMDCh, 10);
        
      Serial.print("0000");
      Serial.print(String(strCMDCh)); 
      Serial.print(" ");
      Serial.print(pinI);
      Serial.print(" ");
      Serial.println(val);
    }
    else if (sOptTypeVal[OT_RETURN_AR]==cmdCH)
    {
      int pinI = _Str2PinIndex(cmdParams[1]);
      int pin = _Str2Pin(cmdParams[1]);
      int val = analogRead(pin);
      
      unsigned char cmdCh = sOptTypeVal[OT_RETURN_AR];
      char strCMDCh[32];
      memset(strCMDCh, 0, 32);
      itoa(cmdCh, strCMDCh, 10);
        
      Serial.print("0000");
      Serial.print(String(strCMDCh)); 
      Serial.print(" ");
      Serial.print(pinI);
      Serial.print(" ");
      Serial.println(val);
    }
    else if (sOptTypeVal[OT_SERIAL_BEGIN]== cmdCH)
    {
        long bt = _Str2Long(cmdParams[1]);
        serialBegin((unsigned long)bt);
    }
    else if (sOptTypeVal[OT_MKSERIAL_SEND] == cmdCH)
    {
      String str;
      int i=1;
      for (; i<cmdIndexTemp; i++)
      {
        str += cmdParams[i];
        if (i < cmdIndexTemp-1){
          str += " ";
        }
      }
      
      _SerialReceivedString_MK = str;
      
      unsigned char cmdCh = sOptTypeVal[OT_RETURN_MKSERIAL];
      char strCMDCh[32];
      memset(strCMDCh, 0, 32);
      itoa(cmdCh, strCMDCh, 10);
        
      Serial.print("0000");
      Serial.print(String(strCMDCh)); 
      Serial.print(" ");
      Serial.println(_SerialReceivedString_MK);
    }
    else if (sOptTypeVal[OT_MKSERAL_CLEAR] == cmdCH){
      clearSerialReceivedString_MK();
    }
    else
    {
      if (Objects)
      {
      
        mkObject *obj = 0;

#if defined MK_DHT
        obj = mkDHT::Create(cmdCH, cmdParams);
        if (obj) Objects->put(obj->NameUni, obj);
#endif
        
        obj = mkDist::Create(cmdCH, cmdParams);
        if (obj) Objects->put(obj->NameUni, obj);

#if defined MK_XH711
        obj = mkHX711::Create(cmdCH, cmdParams);
        if (obj) Objects->put(obj->NameUni, obj);
#endif
#if defined MK_IR
        obj = mkIR::Create(cmdCH, cmdParams);
        if (obj) Objects->put(obj->NameUni, obj);
#endif
#if defined MK_LEDMATRIX
        obj = mkLEDMatrix::Create(cmdCH, cmdParams);
        if (obj) Objects->put(obj->NameUni, obj);
#endif
#if defined MK_MOTO
        obj = mkMoto::Create(cmdCH, cmdParams);
        if (obj) Objects->put(obj->NameUni, obj);
#endif
#if defined MK_MP3
        obj = mkMP3::Create(cmdCH, cmdParams);
        if (obj) Objects->put(obj->NameUni, obj);
#endif
#if defined MK_RCSWITCH
        obj = mkRCSwitch::Create(cmdCH, cmdParams);
        if (obj) Objects->put(obj->NameUni, obj);
#endif
#if defined MK_RGBLED
        obj = mkRGBLed::Create(cmdCH, cmdParams);
        if (obj) Objects->put(obj->NameUni, obj);
#endif
#if defined MK_SCREEN_I2C
        obj = mkScreenI2C::Create(cmdCH, cmdParams);
        if (obj) Objects->put(obj->NameUni, obj);
#endif
#if defined MK_SEGMENT
        obj = mkSegment::Create(cmdCH, cmdParams);
        if (obj) Objects->put(obj->NameUni, obj);
#endif
#if defined MK_SERVO
        obj = mkServo::Create(cmdCH, cmdParams);
        if (obj) Objects->put(obj->NameUni, obj);
#endif
#if defined MK_DUST
        obj = mkDust::Create(cmdCH, cmdParams);
        if (obj) Objects->put(obj->NameUni, obj);
#endif

        String na = cmdParams[1];

        int sz = Objects->size();
        int i=0;
        for (i=0; i<sz; i++)
        {
          mkObject *obj = Objects->getData(i);
          if (obj)
          {
            String nameUni = obj->GenName(na);
            if (nameUni==obj->NameUni)
            {
              obj->OnCMD(cmdCH, cmdParams);
            }
          }
        }
      }
    }
  }
}
//----------------------------------------------------------------------------
int mkArduino::_Str2IO(String &str)
{
  char ch = atoi(str.c_str());
  if (0 == ch)
    return INPUT;
  else if (1 == ch)
    return OUTPUT;
  else if (2 == ch)
    return INPUT_PULLUP;
  
  return OUTPUT;
}
//----------------------------------------------------------------------------
int mkArduino::_Str2PinIndex(String &str)
{
  int pinVal = atoi(str.c_str());
  if (0 <= pinVal && pinVal <= P_21)
    return pinVal;
  else if (MK_A0 <= pinVal)
    return pinVal - MK_A0 + P_A0;

  return 0;
}
//----------------------------------------------------------------------------
int mkArduino::_Str2Pin(String &str)
{
  int pinVal = atoi(str.c_str());
  if (0 <= pinVal && pinVal <= P_21)
    return pinVal;
  else if (MK_A0 <= pinVal)
    return (pinVal - MK_A0) + A0;

  return 0;
}
//----------------------------------------------------------------------------
bool mkArduino::_Str2Bool(String &str)
{
  if (!String("0").compareTo(str))
    return false;

  return true;
}
//----------------------------------------------------------------------------
int mkArduino::_Str2Int(String &str)
{
  int iVal = atoi(str.c_str());
  return iVal; 
}
//----------------------------------------------------------------------------
float mkArduino::_Str2Float(String &str)
{
  float fVal = (float)atof(str.c_str());
  return fVal;
}
//----------------------------------------------------------------------------
long mkArduino::_Str2Long(String &str)
{
  long lVal = (float)atol(str.c_str());
  return lVal;
}
//----------------------------------------------------------------------------
int mkArduino::_Str2DirType(String &str)
{
  return atoi(str.c_str());
}
//----------------------------------------------------------------------------
int mkArduino::_Str2SimpleDirType(String &str)
{
  return atoi(str.c_str());
}
//----------------------------------------------------------------------------
