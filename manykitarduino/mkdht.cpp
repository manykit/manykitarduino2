// mkdht.cpp

#include "mkdht.h"

#if defined MK_DHT

#include "mkarduino.h"

//----------------------------------------------------------------------------
mkDHT::mkDHT()
{
}
//----------------------------------------------------------------------------
mkDHT::~mkDHT()
{
}
//----------------------------------------------------------------------------
mkObject *mkDHT::Create(unsigned char cmdCH, String *cmdParams)
{
    if (mkArduino::sOptTypeVal[OT_DHT_I]==cmdCH)
    {
        String name = cmdParams[1];
        if (!mkArduino::ardu->GetObjectByNameUni(sGenName(name)))
        {
          mkObject *obj = new mkDHT();
          obj->OnCreate(name, cmdCH, cmdParams);
          return obj;
        }
    }

    return 0;
}
//----------------------------------------------------------------------------
void mkDHT::OnSendBack()
{
    SendbackHumidity();
}
//----------------------------------------------------------------------------
void mkDHT::OnCMD(unsigned char cmdCH, String *cmdParams)
{
  if (mkArduino::sOptTypeVal[OT_DHT_I]==cmdCH)
  {
    int pin = mkArduino::_Str2Pin(cmdParams[2]);
    Init(pin);
  }
}
//----------------------------------------------------------------------------
void mkDHT::Init(int pin)
{
  pinMode(pin, OUTPUT);

  mDHT = ManyKit_DHT(pin, MK_DHTTYPE);
  mDHT.begin();
  
  mIsInitedDHT = true;
  mTemperature = 0.0f;
  mHumidity = 0.0f;
}
//----------------------------------------------------------------------------
float mkDHT::GetTemperature() const
{
  return mTemperature;
}
//----------------------------------------------------------------------------
float mkDHT::GetHumidity() const
{
  return mHumidity;
}
//----------------------------------------------------------------------------
void mkDHT::SendbackHumidity()
{
  if (mIsInitedDHT)
  {
    float temp = mDHT.readTemperature();
    float humi = mDHT.readHumidity();

    if (!isnan(humi) && !isnan(temp))
    {
      mTemperature = temp;
      mHumidity = humi;

      unsigned char cmdCh = mkArduino::sOptTypeVal[OT_RETURN_DHTTEMP];
      char strCMDCh[32];
      memset(strCMDCh, 0, 32);
      itoa(cmdCh, strCMDCh, 10);

      Serial.print("0000");
      Serial.print(String(strCMDCh));
      Serial.print(" ");
      Serial.print(Name);
      Serial.print(" ");
      Serial.print(temp);
      Serial.print(" ");
      Serial.println(humi);
    }
  }
}
//----------------------------------------------------------------------------

#endif
