// mkdist.h

#ifndef MKDIST_H
#define MKDIST_H

#include "mkarduinodef.h"

#if defined MK_DIST

#include "mkobject.h"

class mkDist : public mkObject
{
public:
    mkDist();
    virtual ~mkDist();

    static String sGenName(const String &t) { return "2_" + t; }
    virtual String GenName(const String &t) { return "2_" + t; }

    static mkObject *Create(unsigned char cmdCH, String *cmdParams);

    virtual void OnSendBack();
    virtual void OnCMD(unsigned char cmdCH, String *cmdParams);

    void Init(int pinTrig, int pinEcho);
    void Test0();
    void Test();
    float GetDist() const;

private:
    int mPinDistTrigger;
    int mPinDistEcho;
    float mDist;
    int mDistCheckLastTime;
};

#endif

#endif
