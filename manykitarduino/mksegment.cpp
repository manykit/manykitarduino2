// mkledmatrix.cpp

#include "mksegment.h"

#if defined MK_SEGMENT

#include "mkarduino.h"

//----------------------------------------------------------------------------
mkSegment::mkSegment()
{
}
//----------------------------------------------------------------------------
mkSegment::~mkSegment()
{
}
//----------------------------------------------------------------------------
mkObject *mkSegment::Create(unsigned char cmdCH, String *cmdParams)
{
    if (mkArduino::sOptTypeVal[OT_SEGMENT_I]==cmdCH)
    {
        String name = cmdParams[1];
        if (!mkArduino::ardu->GetObjectByNameUni(sGenName(name)))
        {
          mkObject *obj = new mkSegment();
          obj->OnCreate(name, cmdCH, cmdParams);
          return obj;
        }
    }

    return 0;
}
//----------------------------------------------------------------------------
void mkSegment::Update()
{
}
//----------------------------------------------------------------------------
void mkSegment::OnCMD(unsigned char cmdCH, String *cmdParams)
{
  if (mkArduino::sOptTypeVal[OT_SEGMENT_I]==cmdCH)
  {
    int pinClk = mkArduino::_Str2Pin(cmdParams[2]);
    int pinData = mkArduino::_Str2Pin(cmdParams[3]);
    Init(pinClk, pinData);
  }
  else if (mkArduino::sOptTypeVal[OT_SEGMENT_BRIGHTNESS]==cmdCH)
  {
    int val = mkArduino::_Str2Int(cmdParams[2]);
    SetBrightness(val);
  }
  else if (mkArduino::sOptTypeVal[OT_SEGMENT_CLEAR]==cmdCH)
  {
    Clear();
  }
  else if (mkArduino::sOptTypeVal[OT_SEGMENT_DISPLAY]==cmdCH)
  {
    int type = mkArduino::_Str2Int(cmdParams[2]);
    float val = mkArduino::_Str2Float(cmdParams[3]);
    if (1 == type)
    {
      DisplayInt((int)val);
    }
    else
    {
      DisplayFloat(val);
    }
  }
}
//----------------------------------------------------------------------------
void mkSegment::Init(int clkPin, int dataPin)
{
    mSegmentDisplay = SegmentDisplay(clkPin, dataPin);
}
//----------------------------------------------------------------------------
void mkSegment::SetBrightness(int brightness)
{
    mSegmentDisplay.setBrightness((uint8_t)brightness);
}
//----------------------------------------------------------------------------
void mkSegment::Clear()
{
    mSegmentDisplay.clearDisplay();
}
//----------------------------------------------------------------------------
void mkSegment::DisplayInt(int val)
{
    mSegmentDisplay.display((int16_t)val);
}
//----------------------------------------------------------------------------
void mkSegment::DisplayFloat(float val)
{
    mSegmentDisplay.display(val);
}
//----------------------------------------------------------------------------

#endif