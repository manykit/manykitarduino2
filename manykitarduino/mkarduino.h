// mkarduino.h

#ifndef MKARDUINO_H
#define MKARDUINO_H

#include "mkarduinodef.h"
#include "mktimer.h"
#include "mksimplemap.h"
#include "mkobject.h"

class mkArduino
{
public:
  mkArduino();
  ~mkArduino();

  static String Useage;
  static mkArduino *ardu;
  static char PinStr[P_MAX_TYPE];
  static unsigned char sOptTypeVal[OT_MAX_TYPE];

  SimpleMap<String, mkObject* > *Objects;
  mkObject *GetObjectByNameUni(const String &nameuni);
  mkObject *moto;

  String RecvStr;
  void OnCMD(String *cmdParams, String &cmdStr);

  void servoInit(const String &name, int pin);
  void servoWrite(const String &name, int val);

  void temptureInit(const String &name, int pin);
  float temptureGetTemperature(const String &name) const;
  float temptureGetHumidity(const String &name) const;

  void distInit(const String &name, int pinTrig, int pinEcho);
  float distMeasure(const String &name);

  void dustInit(const String &name, int pinLed, int pinSignal);
  void dustTest(const String &name);
  float dustGetOutputV(const String &name) const;
  float dustGetUgm3(const String &name) const;
  float dustGetAqi(const String &name) const;
  int dustGetGrade(const String &name) const;
  String dustGetGradeInfo(const String &name) const;

  void irReceiverInit(const String &name, int pin);
  float irGetReceivedValue(const String &name) const;
  void irSend(const String &name, int val);

  void ledmatrixInit(const String &name, int sckPin, int dinPin);
  void ledmatrixSetBrightness(const String &name, int brightness);
  void ledmatrixClearScreen();
  void ledmatrixSetLightPos(const String &name, int8_t x, int8_t y, int width, bool onOff);

  void motoInit4567(const String &name);
  void motoInit10111213(const String &name);
  void motoInit298N(const String &name, int pinL, int pinL1, int pinLS, int pinR, int pinR1, int pinRS);
  void motoPidInit(const String &name, int encorderLA, int encorderLB, int encorderRA, int encorderRB);
  void motoPidShutdown(const String &name);
  void motoRun(const String &name, int leftright, int dir, int spd);
  void motoRunLeftRight(const String &name, int dir, int spd);
  void motoRunLeft(const String &name, int dir, int spd);
  void motoRunRight(const String &name, int dir, int spd);
  void motoStop(const String &name);

  void mp3Init(const String &name, int pinR, int pinT);
  void mp3Do(const String &name, int type);
  void mp3FolderPlay(const String &name, int folder, int index);
  void mp3SetVolume(const String &name, int val);

  void rgbledInit(const String &name, int pin, int num);
  void rgbledSetColor(const String &name, int index, int r, int g, int b);

  void lci2cInit(const String &name, int addr = 0x3F, int numCols=16, int numRows=2);
  void lci2cDo(const String &name, int doType);
  void lci2cSetCursor(const String &name, int col, int row);
  void lci2cSetBackLight(const String &name, int val);
  void lci2cPrint(const String &name, const String &val);
  void lci2cPrintByte(const String &name, int selfCreateCharIndex);

  void segmentInit(const String &name, int clkPin, int dataPin);
  void segmentSetBrightness(const String &name, int brightness);
  void segmentClear(const String &name);
  void segmentDisplayInt(const String &name, int val);
  void segmentDisplayFloat(const String &name, float val);

public:
  static String _I2Str(int val);
  static String _F2Str(float val);
  static int _Str2IO(String &str);
  static int _Str2PinIndex(String &str);
  static int _Str2Pin(String &str);
  static bool _Str2Bool(String &str);
  static int _Str2Int(String &str);
  static float _Str2Float(String &str);
  static long _Str2Long(String &str);
  static int _Str2DirType(String &str);
  static int _Str2SimpleDirType(String &str);

  char *mpCMD;
  String mCmdParams[NumCMDParams];

public:
  void Init();
  void Tick();

private:
  void _SendVersion();
  void _SendNetID();
  void _Delay(float seconds);
  void _EmptyLoop();
  void _SetTimeMe();

  unsigned long mSettedTimeMe;
  int mNetID;
  unsigned long mLastSendVersionTime;
  Timer mTimer;

public:
  void serialBegin(unsigned long baudrate);
  void serialPrintln(String str);
  String getSerialReceived();
  void clearSerialReceived();

  void serialSend_MK(String str);
  String getSerialReceivedString_MK();
  void clearSerialReceivedString_MK(); 

private:
  String _SerialReceivedString_MK;
};

extern mkArduino mk;

#endif
