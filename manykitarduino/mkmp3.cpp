// mkmp3.cpp

#include "mkmp3.h"

#if defined MK_MP3

#include "mkarduino.h"

//----------------------------------------------------------------------------
mkMP3::mkMP3()
{
}
//----------------------------------------------------------------------------
mkMP3::~mkMP3()
{
}
//----------------------------------------------------------------------------
mkObject *mkMP3::Create(unsigned char cmdCH, String *cmdParams)
{
    if (mkArduino::sOptTypeVal[OT_MP3_INIT]==cmdCH)
    {
        String name = cmdParams[1];
        if (!mkArduino::ardu->GetObjectByNameUni(sGenName(name)))
        {
          mkObject *obj = new mkMP3();
          obj->OnCreate(name, cmdCH, cmdParams);
          return obj;
        }
    }

    return 0;
}
//----------------------------------------------------------------------------
void mkMP3::OnCMD(unsigned char cmdCH, String *cmdParams)
{
  if (mkArduino::sOptTypeVal[OT_MP3_INIT]==cmdCH)
  {
    int pinR = mkArduino::_Str2Pin(cmdParams[2]);
    int pinT = mkArduino::_Str2Pin(cmdParams[3]);
    MP3Init(pinR, pinT);
  }
  else if (mkArduino::sOptTypeVal[OT_MP3_DO]==cmdCH)
  {
    Mp3PlayType type = (Mp3PlayType)mkArduino::_Str2Int(cmdParams[2]);
    MP3Do(type);
  }
}
//----------------------------------------------------------------------------
void mkMP3::Init(int pinR, int pinT)
{
  mMP3 = MP3(pinR, pinT, false);
  mMP3.begin(9600);
  mMP3.SelectPlayerDevice(0x02);
  mMP3.SetVolume(15);
}
//----------------------------------------------------------------------------
void mkMP3::Do(Mp3PlayType type)
{
  if (type == MPT_PLAY)
  {
    mMP3.Play();
  }
  else if (type == MPT_PAUSE)
  {
    mMP3.PlayPause();
  }
  else if (type == MPT_STOP)
  {
    mMP3.PlayStop();
  }
  else if (type == MPT_NEXT)
  {
    mMP3.PlayNext();
  }
  else if (type == MPT_BEFORE)
  {
    mMP3.PlayPrevious();
  }
  else if (type == MPT_RANDOM)
  {
    mMP3.PlayRandom();
  }
  else if (type == MPT_LOOP_SINGLE)
  {
    mMP3.PlaySingleLoopOpen();
  }
  else if (type == MPT_LOOP_SINGLE_CLOSE)
  {
    mMP3.PlaySingleLoopClose();
  }
  else if (type == MPT_LOOP_ALL)
  {
    mMP3.PlayAllLoopOpen();
  }
  else if (type == MPT_LOOP_ALL_CLOSE)
  {
    mMP3.PlayAllLoopClose();
  }
  else if (type == MPT_VOLUME_INCREASE)
  {
    mMP3.IncreaseVolume();
  }
  else if (type == MPT_VOLUME_DECREASE)
  {
    mMP3.DecreaseVolume();
  }
}
//----------------------------------------------------------------------------
void mkMP3::FolderPlay(int folder, int index)
{
    mMP3.PlayFolderIndex(folder, index);
}
//----------------------------------------------------------------------------
void mkMP3::SetVolume(int val)
{
    mMP3.SetVolume(val);
}
//----------------------------------------------------------------------------

#endif