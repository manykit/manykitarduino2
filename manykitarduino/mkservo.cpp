// mkservo.cpp

#include "mkservo.h"

#if defined MK_SERVO

#include "mkarduino.h"

//----------------------------------------------------------------------------
mkServo::mkServo()
{
}
//----------------------------------------------------------------------------
mkServo::~mkServo()
{
}
//----------------------------------------------------------------------------
mkObject *mkServo::Create(unsigned char cmdCH, String *cmdParams)
{
    if (mkArduino::sOptTypeVal[OT_SVR_I]==cmdCH)
    {
        String name = cmdParams[1];
        if (!mkArduino::ardu->GetObjectByNameUni(sGenName(name)))
        {
            mkObject *obj = new mkServo();
            obj->OnCreate(name, cmdCH, cmdParams);
            return obj;
        }
    }

    return 0;
}
//----------------------------------------------------------------------------
void mkServo::OnCMD(unsigned char cmdCH, String *cmdParams)
{
    if (mkArduino::sOptTypeVal[OT_SVR_I]==cmdCH)
    {
        int pin = mkArduino::_Str2Pin(cmdParams[2]);

        mServo.attach(pin);
    }
    else if (mkArduino::sOptTypeVal[OT_SVR_W]==cmdCH)
    {
        int val = mkArduino::_Str2Int(cmdParams[2]);

        mServo.write(val);
    }
}
//----------------------------------------------------------------------------
void mkServo::Init(int pin)
{
    mServo.attach(pin);
}
//----------------------------------------------------------------------------
void mkServo::Write(int val)
{
    mServo.write(val);
}
//----------------------------------------------------------------------------

#endif