// mkhx711.h

#ifndef MKHX711_H
#define MKHX711_H

#include "mkarduinodef.h"

#if defined MK_XH711

#include "mkobject.h"
#include "_mkhx711.h"

class mkHX711 : public mkObject
{
public:
    mkHX711();
    virtual ~mkHX711();

    static String sGenName(const String &t) { return "3_" + t; }
    virtual String GenName(const String &t) { return "3_" + t; }

    static mkObject *Create(unsigned char cmdCH, String *cmdParams);

    virtual void OnCMD(unsigned char cmdCH, String *cmdParams);

    void HX711Init(int pinOut, int pinClk);
    float _ReadHX711();
    void _HXSend(float val);

private:
    HX711 mXH711;
};

#endif

#endif
