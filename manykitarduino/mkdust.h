// mkdust.h

#ifndef MKDUST_H
#define MKDUST_H

#include "mkarduinodef.h"

#if defined MK_DUST

#include "mkobject.h"

class GP2Y1010AU0F;

class mkDust : public mkObject
{
public:
    mkDust();
    virtual ~mkDust();

    static String sGenName(const String &t) { return "13_" + t; }
    virtual String GenName(const String &t) { return "13_" + t; }

    static mkObject *Create(unsigned char cmdCH, String *cmdParams);

    virtual void OnSendBack();
    virtual void OnCMD(unsigned char cmdCH, String *cmdParams);

    void Init(int pinLed, int pinOutput);
    void Test0();
    void Test();
    
    float GetOutputV() const;
    float GetUgm3() const;
    float GetAqi() const;
    int GetGrade() const;
    const String &GetGradeInfo() const;

private:
    int mDustCheckLastTime;
    GP2Y1010AU0F *mGP2Y1010AU0F;
    double mOutputV;
    double mUgm3;
    double mAqi;
    int mGrade;
    String mGradeInfo;
};

#endif

#endif
