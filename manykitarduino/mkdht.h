// mkdht.h

#ifndef MKDHT_H
#define MKDHT_H

#include "mkarduinodef.h"

#if defined MK_DHT

#include "mkobject.h"
#include "_mkdht.h"

#define MK_DHTTYPE DHT11 

class mkDHT : public mkObject
{
public:
    mkDHT();
    virtual ~mkDHT();

    static String sGenName(const String &t) { return "1_" + t; }
    virtual String GenName(const String &t) { return "1_" + t; }

    static mkObject *Create(unsigned char cmdCH, String *cmdParams);

    virtual void OnSendBack();
    virtual void OnCMD(unsigned char cmdCH, String *cmdParams);

    void Init(int pin);
    float GetTemperature() const;
    float GetHumidity() const;

    void SendbackHumidity();

private:
    void _DHTSendTemperatureHumidity();

    int mPin;
    ManyKit_DHT mDHT;
    bool mIsInitedDHT;
    float mTemperature;
    float mHumidity;
};

#endif

#endif
