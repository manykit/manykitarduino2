// mkledmatrix.h

#ifndef MKLEDMATRIX_H
#define MKLEDMATRIX_H

#include "mkarduinodef.h"

#if defined MK_LEDMATRIX
#include "mkobject.h"
#include "_mkledmatrix.h"

class mkLEDMatrix : public mkObject
{
public:
    mkLEDMatrix();
    virtual ~mkLEDMatrix();

    static String sGenName(const String &t) { return "5_" + t; }
    virtual String GenName(const String &t) { return "5_" + t; }

    static mkObject *Create(unsigned char cmdCH, String *cmdParams);

    virtual void OnCMD(unsigned char cmdCH, String *cmdParams);

    void Init(int sckPin, int dinPin);
    void SetBrightness(int brightness);
    void ClearScreen();
    void SetColorIndex(int iColor_Number);
  
    void DrawBitmap(int8_t x, int8_t y, uint8_t bitmap_Width, uint8_t *bitmap);
    void DrawStr(int16_t x_position, int8_t y_position, const char *str);
    void ShowClock(uint8_t hour, uint8_t minute, bool isPointON = true);
    void ShowNum(float value,uint8_t val= 3);

    void SetLightPos(int8_t x, int8_t y, int width, bool onOff);
    
private:

   LEDMatrix mLEDMatrix;
};
#endif

#endif
