// mkobject.h

#ifndef MKOBJECT_H
#define MKOBJECT_H

#include "mkarduinodef.h"

class mkObject
{
public:
    mkObject();
    virtual ~mkObject();
    
    virtual String GenName(const String &t);
    void OnCreate(const String &name);
    void OnCreate(const String &name, unsigned char cmdCH, String *cmdParams);

    void SetSendBackTime(int miliSeconds);
    virtual void Update();
    virtual void OnSendBack();
    virtual void OnCMD(unsigned char cmdCH, String *cmdParams);

    String Name;
    String NameUni;

    String Type;

private:
    int mSendBackTime;
    int mSendBackLastTime;
};

#endif