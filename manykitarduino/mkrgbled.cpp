// mkrgbled.cpp

#include "mkrgbled.h"

#if defined MK_RGBLED

#include "mkarduino.h"

//----------------------------------------------------------------------------
mkRGBLed::mkRGBLed()
{
}
//----------------------------------------------------------------------------
mkRGBLed::~mkRGBLed()
{
}
//----------------------------------------------------------------------------
mkObject *mkRGBLed::Create(unsigned char cmdCH, String *cmdParams)
{
    if (mkArduino::sOptTypeVal[OT_LEDSTRIP_I]==cmdCH)
    {
        String name = cmdParams[1];
        if (!mkArduino::ardu->GetObjectByNameUni(sGenName(name)))
        {
          mkObject *obj = new mkRGBLed();
          obj->OnCreate(name, cmdCH, cmdParams);
          return obj;
        }

    }

    return 0;
}
//----------------------------------------------------------------------------
void mkRGBLed::Update()
{
}
//----------------------------------------------------------------------------
void mkRGBLed::OnCMD(unsigned char cmdCH, String *cmdParams)
{
  if (mkArduino::sOptTypeVal[OT_LEDSTRIP_I]==cmdCH)
  {
    int pin = mkArduino::_Str2Pin(cmdParams[2]);
    int num = mkArduino::_Str2Int(cmdParams[3]);
    Init(pin, num);
  }
  else if (mkArduino::sOptTypeVal[OT_LEDSTRIP_SET]==cmdCH)
  {
    int index = mkArduino::_Str2Int(cmdParams[2]);
    int r = mkArduino::_Str2Int(cmdParams[3]);
    int g = mkArduino::_Str2Int(cmdParams[4]);
    int b = mkArduino::_Str2Int(cmdParams[5]);
    SetColor(index, r, g, b);  
  }
}
//----------------------------------------------------------------------------
void mkRGBLed::Init(int pin, int num)
{
  mWS2812 = WS2812(num);
  mWS2812.setOutput(pin);
}
//----------------------------------------------------------------------------
void mkRGBLed::SetColor(int index, int r, int g, int b)
{
  cRGB c;
  c.r = r;
  c.g = g;
  c.b = b;
  mWS2812.set_crgb_at(index, c);
  mWS2812.sync();
}
//----------------------------------------------------------------------------

#endif