// mkmoto.h

#ifndef MKMOTO_H
#define MKMOTO_H

#include "mkarduinodef.h"

#if defined MK_MOTO

#include "mkobject.h"

#if defined MK_PID
#include "_mkpid.h"
#endif

class mkMoto : public mkObject
{
public:
    mkMoto();
    virtual ~mkMoto();

    static String sGenName(const String &t) { return "6_" + t; }
    virtual String GenName(const String &t) { return "6_" + t; }

    static mkObject *Create(unsigned char cmdCH, String *cmdParams);

    virtual void OnSendBack();
    virtual void OnCMD(unsigned char cmdCH, String *cmdParams);

    enum MotoMode
    {
        MM_BOARD,
        MM_298N,
        MM_MAX_TYPE
    };

    void Init4567();
    void Init10111213();
    void Init298N(int pinL, int pinL1, int pinLS, int pinR, int pinR1, int pinRS);

    void Run(int leftright, int dir, int spd);
    void RunLeftRight(int dir, int spd);
    void RunLeft(int dir, int spd);
    void RunRight(int dir, int spd);

    void Stop();
 
    void PidInit(int encorderLA, int encorderLB, int encorderRA, int encorderRB);
    void PidShutdown();
    float GetSpeedLeft() const;
    float GetSpeedRight() const;
    int GetDirectionLeft() const;
    int GetDirectionRight() const;

public:
    void _SendbackSpeed();

    MotoMode mMotoMode;

    int mPinL0;
    int mPinL1;
    int mPinLSpeed;
    int mPinR0;
    int mPinR1;
    int mPinRSpeed;
    bool mIsUseSpeedEncorder;

    int mPinEncroderLA;
    int mPinEncroderLB;
    int mPinEncroderRA;
    int mPinEncroderRB;
    double mSetPoint_L;
    double mSetPoint_R;

#if defined MK_PID
    unsigned long mLastSendTime;
    bool mMotoResultL;
    bool mMotoResultR;
    PID *mPID;
    PID *mPID1;
    byte mEncoder0PinALastL;
    byte mEncoder0PinALastR;
    int mDurationLTemp;
    int mDurationRTemp;
    double mAbsDurationL;
    double mAbsDurationR;
    double mValOutputL;
    double mValOutputR;
    bool mDirectionL;
    bool mDirectionR;
    bool mIsStopL;
    bool mIsStopR;
#endif
};

#endif

#endif
